<?php
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'gustavo',
		'password' => 'root',
		'database' => 'lmabogados',
		'encoding' => 'utf8'
	);
}
