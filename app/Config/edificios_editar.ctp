<?php $this -> append( "activeEdificios" ); echo "active"; $this -> end(); ?>
<div id="edificios" class="editar">
    <div class="block-header">
        <div class="header-section">
            <h1><i class="fa fa-users animation-expandUp"></i>Edificios<br><small>Nuevo edificio</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><i class="fa fa-th"></i></li>
        <li>Ajustes</li>
        <li>Edificios</li>
        <li><a href="">Nuevo edificio</a></li>
    </ul>
    <div class="table-options clearfix">
        <?php echo $this -> Html -> link( "<i class='fa fa-arrow-left'></i> Volver", array( "controller" => "administrador", "action" => "edificios" ), array( "class" => "btn btn-sm btn-success", "escape" => false ) ); ?>
    </div>
    <form action="<?php echo $this -> webroot . "extraAdministrador/save_edificios"; ?>" method="post" class="form-horizontal">
        <div class="block">
            <div class="block-title"><h2>Datos del edificio</h2></div>
            <div class="form-group">
                <label class="col-xs-6">Nombre</label>
                <label class="col-xs-6">Contacto</label>
                <div class="col-xs-6">
                    <input type="text" name="nombre" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "nombre" ]; ?>" />
                </div>
                <div class="col-xs-6">
                    <input type="text" name="contacto" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "contacto" ]; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-6">Dirección</label>
                <label class="col-xs-6">Teléfono fijo</label>
                <div class="col-xs-6">
                    <input type="text" name="direccion" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "direccion" ]; ?>" />
                </div>
                <div class="col-xs-6">
                    <input type="text" name="telefono" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "telefono" ]; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-6">Ciudad</label>
                <label class="col-xs-6">Celular</label>
                <div class="col-xs-6">
                    <select name="provincias_id" class="form-control is" required="required" disabled>
                        <option value="0">Seleccionar ciudad</option>
                        <?php foreach( $checkProvincias as $key => $value ) { ?>
                            <option value="<?php echo $value[ "Provincia" ][ "id" ]; ?>" <?php echo $value[ "Provincia" ][ "id" ] == $checkRow[ "Edificio" ][ "provincias_id" ] ? "selected" : ""; ?>><?php echo $value[ "Provincia" ][ "provincia_nombre" ]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-6">
                    <input type="text" name="celular" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "celular" ]; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-6">Comuna</label>
                <label class="col-xs-6">Email</label>
                <div class="col-xs-6">
                    <select name="comunas_id" class="form-control is" required="required" disabled>
                        <option value="0">Seleccionar comuna</option>
                        <?php foreach( $checkComunas as $key => $value ) { ?>
                            <option value="<?php echo $value[ "Comuna" ][ "id" ]; ?>" <?php echo $value[ "Comuna" ][ "id" ] == $checkRow[ "Edificio" ][ "comunas_id" ] ? "selected" : ""; ?>><?php echo $value[ "Comuna" ][ "comuna_nombre" ]; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-6">
                    <input type="text" name="email" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "email" ]; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-12">Código PINMATE</label>
                <div class="col-xs-6">
                    <input type="text" name="codigo_pinmate" class="form-control is" required="required" disabled value="<?php echo $checkRow[ "Edificio" ][ "codigo_pinmate" ]; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-12">Observaciones</label>
                <div class="col-xs-6">
                    <textarea name="observacion" class="form-control" disabled><?php echo $checkRow[ "Edificio" ][ "observacion" ]; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="btn-group pull-right">
                        <input type="submit" class="hidden" />
                        <input type="hidden" name="id" value="<?php echo $checkRow[ "Edificio" ][ "id" ]; ?>" />
                        <button type="button" class="btn btn-success btn-editar">Editar datos</button>
                        <button type="button" class="btn btn-success disabled btn-save">Guardar cambios</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $this -> append( "script" ); ?>
    <script>
        $(function() {
            AdminApp.edificios();
        });
    </script>
<?php $this -> end(); ?>