<?php
App::uses('AppController', 'Controller');
/**
 * Administrador Controller
 *
 * @property Administrador $Administrador
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class AdministradorController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array("Categoria", "Footer", "Equipo", "Novedade", "Inicio", "NuestroEstudio", "AreaDePractica", "EquiposDes", "Contacto", "PalabrasMenu", "Palabra" );
	public $components = array('Paginator', 'RequestHandler', 'Session');

    public function beforeFilter() {
        if($this->params["action"] != "login" && $this->params["action"] != "logout") {
            if(!$this->Session->check("Administrador")) {
                $this->redirect(array("action" => "login"));
            }
        }
    }
    public function login() {
        $this->layout = "ajax";
    }

    /** LAYOUTS */
    public function index() {
        $this->layout = "administrador";
        $checkRow = $this -> Novedade -> find( "all", array( "order" => array( "Novedade.created" => "DESC" ) ) );
        $this -> set ( "checkRow", $checkRow );
    }
    public function equipo() {
        $this->layout = "administrador";

        $checkEquipo = $this -> Equipo -> findAllByActive( 1 );
        $checkCategoria = $this -> Categoria -> findAllByActive( 1 );
        $this -> set("checkEquipo", $checkEquipo);
        $this -> set("checkCategoria", $checkCategoria);

    }
    public function footer() {
        $this->layout = "administrador";

        $checkFooter = $this -> Footer -> findAllByActive( 1 );
        $this -> set("checkFooter", $checkFooter);
    }
    public function inicio() {
        $this->layout = "administrador";
        $checkInicio = $this -> Inicio -> findById (1);
        $this -> set( "checkInicio", $checkInicio );
    }
    public function contacto() {
        $this->layout = "administrador";

        $checkContacto = $this -> Contacto -> findById (1);
        $this -> set( "checkContacto", $checkContacto);
    }
    public function area_de_practica() {
        $this->layout = "administrador";

        $checkArea = $this -> AreaDePractica -> findById (1);
        $this -> set( "checkArea", $checkArea );
    }
    public function equipo2() {
        $this->layout = "administrador";

        $checkEquipo = $this -> EquiposDes -> findById (1);
        $this -> set( "checkEquipo", $checkEquipo );
    }

    public function nuestro_estudio() {
        $this->layout = "administrador";

        $checkEstudio = $this -> NuestroEstudio -> findById (1);
        $this -> set( "checkEstudio", $checkEstudio );
    }
    public function palabras_menu() {
        $this->layout = "administrador";

        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $this -> set( "checkPalabras1", $checkPalabras1 );
    }
    public function palabras() {
        $this->layout = "administrador";

        $checkPalabras2 = $this -> Palabra -> findById (1);
        $this -> set( "checkPalabras2", $checkPalabras2 );
    }
}
