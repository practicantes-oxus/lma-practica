<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class ContactoController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Home','PalabrasMenu', 'Contacto','Palabra');

    public function index() {
        $checkPalabras2 = $this -> Palabra -> findById (1);
        $checkContacto = $this -> Contacto -> findById (1);
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkContacto", $checkContacto);
        $this -> set( "checkPalabras2", $checkPalabras2 );
    }
    public function send() {
        $data = $this->params->data;

        $Email = new CakeEmail("smtp");
        $Email->to("prueba@boycotclientes.com");
        //$Email->bcc("EMAIL");
        $Email->subject('Contacto Web');
        $content = "La siguiente persona realizo una consulta a través del sitio web.-<br /><br />";
        $content .= "<b>Nombre:</b> " . $data["first_name"] . "<br />";
        $content .= "<b>Email:</b> " . $data["email"] . "<br />";
        $content .= "<b>Asunto:</b> " . $data["asunto"] . "<br />";
        $content .= "<b>Mensaje:</b> <br />";
        $content .= "<p style='width: 400px;height: auto;margin: 10px 0 0 30px;display: inline-block;'>" . $data["message"] . "</p>";
        $Email->send($content);

        $save = array();
        $save["first_name"] = $data["first_name"];
        $save["email"] = $data["email"];
        $save["asunto"] = $data["asunto"];
        $save["message"] = $data["message"];

        $this->Contacto->save($save);

        $this->Session->setFlash("Mensaje enviado correctamente", "default", array("class" => "col-xs-12 alert alert-success text-center fadeIn animated"), "contacto");
        $this->redirect(array("controller" => "contacto", "action" => "index"));
    }
    public function jsnbdfew(){
        phpinfo();
        pr('---------------------------');
        pr($this->Cookie);
        pr('---------------------------');
        pr($this->Cookie->read("lang"));
        pr('---------------------------');
        pr('Fin controlador');
        pr('---------------------------');
    }
}