<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class EquipoController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Categoria', 'Equipo','PalabrasMenu','EquiposDes','Palabra');

    public function index() {
        $checkEquipo2 = $this -> EquiposDes -> findById (1);
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $checkPalabras2 = $this -> Palabra -> findById (1);
        $checkEquipo = $this -> Equipo -> find('all', [
            'conditions'=> array('active'=>1),
            'order' => array('Equipo.id' => 'ASC')
        ]);
        $checkCategoria = $this -> Categoria -> find( "all" );
        $namesListByCategoria = [];
        $categoriasList = [];
        foreach($checkCategoria as $index => $categoria ){
            $categoriaList[$categoria['Categoria']['id']] = $categoria['Categoria']['titulo_es'];
            $members = $this -> Equipo -> findAllByCategoria( $categoria['Categoria']['id']/*, array('order' => array('Equipo.id' => 'asc'))*/);
            $categoria_id = $categoria['Categoria']['id'];
            $namesListByCategoria[$categoria_id] = [];  
            foreach ($members as $member) {
                $member_id = $member['Equipo']['id'];
                $member_name = $member['Equipo']['titulo_es'];
                $namesListByCategoria[$categoria_id][$member_id]=$member_name;
            }
        }
        $this->set('categoriaList',$categoriaList);
        $this->set('namesListByCategoria',$namesListByCategoria);
        $this->set("checkEquipo", $checkEquipo);
        $this->set("checkCategoria", $checkCategoria);
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkPalabras2", $checkPalabras2 );
        $this -> set( "checkEquipo2", $checkEquipo2 );
    }

    public function get_equipo() {
        $this -> render("/Elements/save");
        if($this -> params -> data["id"] == 0) {
            $checkRow = $this -> Equipo -> find("all");
        } else {
            $checkRow = $this -> Equipo -> findAllByCategoria( $this -> params -> data["id"]);
        }

        echo json_encode($checkRow, JSON_FORCE_OBJECT);
    }
    
    public function send() {
        $data = $this->params->data;

        $Email = new CakeEmail("smtp");
        $Email->to($data["email"]);
        //$Email->bcc("EMAIL");
        $Email->subject('Consulta Web');
        $content = "La siguiente persona realizo una consulta a través del sitio web.-<br /><br />";
        $content .= "<b>Email:</b> " . $data["email"] . "<br />";
        $content .= "<b>Email del consultor:</b> " . $data["email2"] . "<br />";
        $content .= "<b>Asunto:</b> " . $data["asunto"] . "<br />";
        $content .= "<b>Mensaje:</b> <br />";
        $content .= "<p style='width: 400px;height: auto;margin: 10px 0 0 30px;display: inline-block;'>" . $data["message"] . "</p>";
        $Email->send($content);

        $save = array();
        $save["email"] = $data["email"];
        $save["email2"] = $data["email2"];
        $save["asunto"] = $data["asunto"];
        $save["message"] = $data["message"];

        $this->Equipo->save($save);

        $this->Session->setFlash("Mensaje enviado correctamente", "default", array("class" => "col-xs-12 alert alert-success text-center fadeIn animated"), "Equipo");
        $this->redirect(array("controller" => "Equipo", "action" => "index"));
    }

}