<?php
App::uses('AppController', 'Controller');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class EstudioController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Home','PalabrasMenu','NuestroEstudio');

    public function index() {
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $checkEstudio = $this -> NuestroEstudio -> findById (1);
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkEstudio", $checkEstudio );
    }

}