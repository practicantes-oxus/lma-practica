<?php
App::uses('AppController', 'Controller');
/**
 * ExtraAdministrador Controller
 *
 * @property ExtraAdministrador $ExtraAdministrador
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class ExtraAdministradorController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $uses = array("Categoria", "Equipo", "Footer", "Novedade", "Inicio", "NuestroEstudio", "AreaDePractica", "EquiposDes", "Contacto", "PalabrasMenu", "Palabra");
	public $components = array('Paginator', 'RequestHandler', 'Session');

    public function login() {
        $data = $this->params->data;

        $username = "lmadmin";
        $password = "ro=n(v";

        if ($data["username"] == $username && $data["password"] == $password) {
            $this->Session->write("Administrador.active", true);
            $this->redirect(array("controller" => "administrador", "action" => "index"));
        } else {
            $this->Session->setFlash("Error con el usuario o contraseña", "default", array("class" => "col-xs-10 col-xs-push-1 alert alert-danger text-center"), "error_login");
            $this->redirect(array("controller" => "administrador", "action" => "login"));
        }
    }
    public function logout() {
        $this->Session->delete("Administrador");
        $this->redirect(array("controller" => "administrador", "action" => "login"));
    }

    public function save_novedades () {
      $this -> layout = "ajax";
      $this -> render("/Elements/save");
      $data = $this -> params -> data;
      $form = $this->params->form["imagen_home"];
      $form2 = $this->params->form["imagen_interior"];

      $save = array();

      $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
      $save["titulo_es"]= $data["titulo_es"];
      $save["titulo_en"]= $data["titulo_en"];
      $save["titulo_pt"]= $data["titulo_pt"];
      $save["descripcion_es"]= $data["descripcion_es"];
      $save["descripcion_en"]= $data["descripcion_en"];
      $save["descripcion_pt"]= $data["descripcion_pt"];

      if( !empty( $form[ "name" ] ) ) {
          $size = getimagesize( $form[ "tmp_name" ] );
          if( $size[0] == 340 && $size[1] == 124 ) {
              $ext = pathinfo( $form[ "name" ], PATHINFO_EXTENSION );
              $name = time() . "." . $ext;
              move_uploaded_file( $form[ "tmp_name" ], WWW_ROOT . "img" . DS . "novedades" . DS . $name );
              $save[ "imagen_home" ] = $name;
          } else {
              $this->Session->setFlash("Error al cargar la imagen, medidas 340 x 124px", "default", array("class" => "text-center alert alert-danger col-xs-4 col-xs-push-4"), "mensaje_admin");
              $this->redirect(array("controller" => "administrador", "action" => "index"));
          }
      }
      if( !empty( $form2[ "name" ] ) ) {
          $size = getimagesize( $form2[ "tmp_name" ] );
          if( $size[0] == 345 && $size[1] == 218 ) {
              $ext = pathinfo( $form2[ "name" ], PATHINFO_EXTENSION );
              $name = time() . "." . $ext;
              move_uploaded_file( $form2[ "tmp_name" ], WWW_ROOT . "img" . DS . "novedades" . DS . $name );
              $save[ "imagen_interior" ] = $name;
          } else {
              $this->Session->setFlash("Error al cargar la imagen, medidas 345px x 218px", "default", array("class" => "text-center alert alert-danger col-xs-4 col-xs-push-4"), "mensaje_admin");
              $this->redirect(array("controller" => "administrador", "action" => "index"));
          }
      }
      $this -> Novedade -> save($save);
      $this -> redirect(array("controller" =>"administrador", "action" => "index"));

    }
    public function save_equipo() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");

        $data = $this -> params -> data;
        $form = $this->params->form["cv"];
        $form2 = $this->params->form["imagen"];
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["titulo_es"] = $data["titulo_es"];
        $save["titulo_en"] = $data["titulo_es"];
        $save["titulo_pt"] = $data["titulo_es"];
        $save["descripcion_es"] = $data["descripcion_es"];
        $save["descripcion_en"] = $data["descripcion_en"];
        $save["descripcion_pt"] = $data["descripcion_pt"];
        $save["categoria"] = $data["categoria"];

        if( !empty( $form[ "name" ] ) ) {
            $ext = pathinfo( $form[ "name" ], PATHINFO_EXTENSION );
            $name = time() . "." . $ext;
            move_uploaded_file( $form[ "tmp_name" ], WWW_ROOT . "files" . DS . "cv" . DS . $name );
            $save[ "cv" ] = $name;
        } else {
            $save["cv"] = "";
        }
        if( !empty( $form2[ "name" ] ) ) {
            $size = getimagesize( $form2[ "tmp_name" ] );
            if( $size[0] == 251 && $size[1] == 214 ) {
                $ext = pathinfo( $form2[ "name" ], PATHINFO_EXTENSION );
                $name = time() . "." . $ext;
                move_uploaded_file( $form2[ "tmp_name" ], WWW_ROOT . "img" . DS . "equipo" . DS . $name );
                $save[ "imagen" ] = $name;
            } else {
                $this->Session->setFlash("Error al cargar la imagen, medidas 340 x 124px", "default", array("class" => "text-center alert alert-danger col-xs-4 col-xs-push-4"), "mensaje_admin");
                $this->redirect(array("controller" => "administrador", "action" => "index"));
            }
        }
        $this -> Equipo -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "equipo"));
    }
    public function save_categoria() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");

        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["titulo_es"]= $data["titulo_es"];
        $save["titulo_en"]= $data["titulo_en"];
        $save["titulo_pt"]= $data["titulo_pt"];
        $this -> Categoria -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "equipo"));

    }
    public function save_footer() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");

        $data = $this -> params -> data;
        $form = $this->params->form["imagen"];
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["href"] = $data["href"];

        if( !empty( $form[ "name" ] ) ) {
            $size = getimagesize( $form[ "tmp_name" ] );
            if( $size[1] == 35 ) {
                $ext = pathinfo( $form[ "name" ], PATHINFO_EXTENSION );
                $name = time() . "." . $ext;
                move_uploaded_file( $form[ "tmp_name" ], WWW_ROOT . "img" . DS . "footer" . DS . $name );
                $save[ "imagen" ] = $name;
            } else {
                $this->Session->setFlash("Error al cargar la imagen, medidas 159 x 49px", "default", array("class" => "text-center alert alert-danger col-xs-4 col-xs-push-4"), "mensaje_admin");
                $this->redirect(array("controller" => "administrador", "action" => "footer"));
            }
        }
        $this -> Footer -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "footer"));
    }
    public function eliminar() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");

        $data = $this -> params -> data;
        $model = ucfirst($data["type"]);
        $save = array();

        $save["id"] = $data["id"];
        $this -> $model -> delete( $save );

        $continue = str_replace($this->base, "", $this->params->query["continue"]);
        $this->redirect($continue);
    }
    public function save_palabras_menu () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["inicio_es"]= $data["inicio_es"];
        $save["inicio_en"]= $data["inicio_en"];
        $save["inicio_pt"]= $data["inicio_pt"];

        $save["nuestro_estudio_es"]= $data["nuestro_estudio_es"];
        $save["nuestro_estudio_en"]= $data["nuestro_estudio_en"];
        $save["nuestro_estudio_pt"]= $data["nuestro_estudio_pt"];

        $save["area_practica_es"]= $data["area_practica_es"];
        $save["area_practica_en"]= $data["area_practica_en"];
        $save["area_practica_pt"]= $data["area_practica_pt"];

        $save["equipo_es"]= $data["equipo_es"];
        $save["equipo_en"]= $data["equipo_en"];
        $save["equipo_pt"]= $data["equipo_pt"];

        $save["novedades_es"]= $data["novedades_es"];
        $save["novedades_en"]= $data["novedades_en"];
        $save["novedades_pt"]= $data["novedades_pt"];

        $save["contacto_es"]= $data["contacto_es"];
        $save["contacto_en"]= $data["contacto_en"];
        $save["contacto_pt"]= $data["contacto_pt"];

        $this -> PalabrasMenu -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "palabras_menu"));
    }
    public function save_palabras () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["leer_mas_es"]= $data["leer_mas_es"];
        $save["leer_mas_en"]= $data["leer_mas_en"];
        $save["leer_mas_pt"]= $data["leer_mas_pt"];

        $save["haz_click_es"]= $data["haz_click_es"];
        $save["haz_click_en"]= $data["haz_click_en"];
        $save["haz_click_pt"]= $data["haz_click_pt"];

        $save["ver_es"]= $data["ver_es"];
        $save["ver_en"]= $data["ver_en"];
        $save["ver_pt"]= $data["ver_pt"];

        $save["sin_filtro_es"]= $data["sin_filtro_es"];
        $save["sin_filtro_en"]= $data["sin_filtro_en"];
        $save["sin_filtro_pt"]= $data["sin_filtro_pt"];

        $save["nombre_es"]= $data["nombre_es"];
        $save["nombre_en"]= $data["nombre_en"];
        $save["nombre_pt"]= $data["nombre_pt"];

        $save["email_es"]= $data["email_es"];
        $save["email_en"]= $data["email_en"];
        $save["email_pt"]= $data["email_pt"];

        $save["asunto_es"]= $data["asunto_es"];
        $save["asunto_en"]= $data["asunto_en"];
        $save["asunto_pt"]= $data["asunto_pt"];

        $save["mensaje_es"]= $data["mensaje_es"];
        $save["mensaje_en"]= $data["mensaje_en"];
        $save["mensaje_pt"]= $data["mensaje_pt"];

        $save["enviar_es"]= $data["enviar_es"];
        $save["enviar_en"]= $data["enviar_en"];
        $save["enviar_pt"]= $data["enviar_pt"];

        $this -> Palabra -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "palabras"));
    }
    public function save_contacto () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["direccion_es"]= $data["direccion_es"];
        $save["direccion_en"]= $data["direccion_en"];
        $save["direccion_pt"]= $data["direccion_pt"];
        $save["descrip_es"]= $data["descrip_es"];
        $save["descrip_en"]= $data["descrip_en"];
        $save["descrip_pt"]= $data["descrip_pt"];

        $this -> Contacto -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "Contacto"));
    }
    public function save_equipo_desc () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["descrip_es"]= $data["descrip_es"];
        $save["descrip_en"]= $data["descrip_en"];
        $save["descrip_pt"]= $data["descrip_pt"];

        $this -> EquiposDes -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "equipo2"));
    }
    public function save_practica () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["titulo_area_es"]= $data["titulo_area_es"];
        $save["titulo_area_en"]= $data["titulo_area_en"];
        $save["titulo_area_pt"]= $data["titulo_area_pt"];

        $save["area_descrip_es"]= $data["area_descrip_es"];
        $save["area_descrip_en"]= $data["area_descrip_en"];
        $save["area_descrip_pt"]= $data["area_descrip_pt"];

        $save["t1_es"]= $data["t1_es"];
        $save["t1_en"]= $data["t1_en"];
        $save["t1_pt"]= $data["t1_pt"];

        $save["t2_es"]= $data["t2_es"];
        $save["t2_en"]= $data["t2_en"];
        $save["t2_pt"]= $data["t2_pt"];

        $save["t3_es"]= $data["t3_es"];
        $save["t3_en"]= $data["t3_en"];
        $save["t3_pt"]= $data["t3_pt"];

        $save["t4_es"]= $data["t4_es"];
        $save["t4_en"]= $data["t4_en"];
        $save["t4_pt"]= $data["t4_pt"];

        $save["t5_es"]= $data["t5_es"];
        $save["t5_en"]= $data["t5_en"];
        $save["t5_pt"]= $data["t5_pt"];

        $save["t6_es"]= $data["t6_es"];
        $save["t6_en"]= $data["t6_en"];
        $save["t6_pt"]= $data["t6_pt"];

        $save["t7_es"]= $data["t7_es"];
        $save["t7_en"]= $data["t7_en"];
        $save["t7_pt"]= $data["t7_pt"];

        $save["t8_es"]= $data["t8_es"];
        $save["t8_en"]= $data["t8_en"];
        $save["t8_pt"]= $data["t8_pt"];

        $save["t1_des_es"]= $data["t1_des_es"];
        $save["t1_des_en"]= $data["t1_des_en"];
        $save["t1_des_pt"]= $data["t1_des_pt"];

        $save["t2_des_es"]= $data["t2_des_es"];
        $save["t2_des_en"]= $data["t2_des_en"];
        $save["t2_des_pt"]= $data["t2_des_pt"];

        $save["t3_des_es"]= $data["t3_des_es"];
        $save["t3_des_en"]= $data["t3_des_en"];
        $save["t3_des_pt"]= $data["t3_des_pt"];

        $save["t4_des_es"]= $data["t4_des_es"];
        $save["t4_des_en"]= $data["t4_des_en"];
        $save["t4_des_pt"]= $data["t4_des_pt"];

        $save["t5_des_es"]= $data["t5_des_es"];
        $save["t5_des_en"]= $data["t5_des_en"];
        $save["t5_des_pt"]= $data["t5_des_pt"];

        $save["t6_des_es"]= $data["t6_des_es"];
        $save["t6_des_en"]= $data["t6_des_en"];
        $save["t6_des_pt"]= $data["t6_des_pt"];

        $save["t7_des_es"]= $data["t7_des_es"];
        $save["t7_des_en"]= $data["t7_des_en"];
        $save["t7_des_pt"]= $data["t7_des_pt"];

        $save["t8_des_es"]= $data["t8_des_es"];
        $save["t8_des_en"]= $data["t8_des_en"];
        $save["t8_des_pt"]= $data["t8_des_pt"];

        $this -> AreaDePractica -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "area_de_practica"));
    }
    public function save_estudios () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;
        $save["mision_titulo_es"]= $data["mision_titulo_es"];
        $save["mision_titulo_en"]= $data["mision_titulo_en"];
        $save["mision_titulo_pt"]= $data["mision_titulo_pt"];

        $save["mision_descrip_es"]= $data["mision_descrip_es"];
        $save["mision_descrip_en"]= $data["mision_descrip_en"];
        $save["mision_descrip_pt"]= $data["mision_descrip_pt"];

        $save["vision_titulo_es"]= $data["vision_titulo_es"];
        $save["vision_titulo_en"]= $data["vision_titulo_en"];
        $save["vision_titulo_pt"]= $data["vision_titulo_pt"];

        $save["vision_descrip_es"]= $data["vision_descrip_es"];
        $save["vision_descrip_en"]= $data["vision_descrip_en"];
        $save["vision_descrip_pt"]= $data["vision_descrip_pt"];

        $save["n_valores_es"]= $data["n_valores_es"];
        $save["n_valores_en"]= $data["n_valores_en"];
        $save["n_valores_pt"]= $data["n_valores_pt"];

        $save["confianza_es"]= $data["confianza_es"];
        $save["confianza_en"]= $data["confianza_en"];
        $save["confianza_pt"]= $data["confianza_pt"];

        $save["confianza_des_es"]= $data["confianza_des_es"];
        $save["confianza_des_en"]= $data["confianza_des_en"];
        $save["confianza_des_pt"]= $data["confianza_des_pt"];

        $save["eficiencia_es"]= $data["eficiencia_es"];
        $save["eficiencia_en"]= $data["eficiencia_en"];
        $save["eficiencia_pt"]= $data["eficiencia_pt"];

        $save["eficiencia_des_es"]= $data["eficiencia_des_es"];
        $save["eficiencia_des_en"]= $data["eficiencia_des_en"];
        $save["eficiencia_des_pt"]= $data["eficiencia_des_pt"];

        $save["compromiso_es"]= $data["compromiso_es"];
        $save["compromiso_en"]= $data["compromiso_en"];
        $save["compromiso_pt"]= $data["compromiso_pt"];

        $save["compromiso_des_es"]= $data["compromiso_des_es"];
        $save["compromiso_des_en"]= $data["compromiso_des_en"];
        $save["compromiso_des_pt"]= $data["compromiso_des_pt"];

        $save["equipo_es"]= $data["equipo_es"];
        $save["equipo_en"]= $data["equipo_en"];
        $save["equipo_pt"]= $data["equipo_pt"];

        $save["equipo_des_es"]= $data["equipo_des_es"];
        $save["equipo_des_en"]= $data["equipo_des_en"];
        $save["equipo_des_pt"]= $data["equipo_des_pt"];


        $this -> NuestroEstudio -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "nuestro_estudio"));
    }
    public function save_inicio () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $data = $this -> params -> data;
        $save = array();

        $save ["id"] = isset( $data["id"] ) ? $data["id"] : null;

        $save["text_slider_es"]= $data["text_slider_es"];
        $save["text_slider_en"]= $data["text_slider_en"];
        $save["text_slider_pt"]= $data["text_slider_pt"];

        $save["titulo1_es"]= $data["titulo1_es"];
        $save["titulo2_es"]= $data["titulo2_es"];
        $save["titulo3_es"]= $data["titulo3_es"];

        $save["titulo1_en"]= $data["titulo1_en"];
        $save["titulo2_en"]= $data["titulo2_en"];
        $save["titulo3_en"]= $data["titulo3_en"];

        $save["titulo1_pt"]= $data["titulo1_pt"];
        $save["titulo2_pt"]= $data["titulo2_pt"];
        $save["titulo3_pt"]= $data["titulo3_pt"];

        $save["nuestro_estudio_es"]= $data["nuestro_estudio_es"];
        $save["trabajo_bono_es"]= $data["trabajo_bono_es"];
        $save["postulaciones_es"]= $data["postulaciones_es"];

        $save["nuestro_estudio_en"]= $data["nuestro_estudio_en"];
        $save["trabajo_bono_en"]= $data["trabajo_bono_en"];
        $save["postulaciones_en"]= $data["postulaciones_en"];

        $save["nuestro_estudio_pt"]= $data["nuestro_estudio_pt"];
        $save["trabajo_bono_pt"]= $data["trabajo_bono_pt"];
        $save["postulaciones_pt"]= $data["postulaciones_pt"];

        $this -> Inicio -> save( $save );

        $this -> redirect(array("controller" =>"administrador", "action" => "inicio"));
    }
    public function get_categoria() {
        $this -> layout = "ajax";

        return $this -> Categoria -> findById( $this -> params -> named["id"]);

    }
    public function get_footer() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $novedades = $this -> Footer -> findById( $this -> params -> data["id"]);
        echo json_encode($novedades);

    }
    public function get_categoria2() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $checkCategoria = $this -> Categoria -> findById( $this -> params -> data["id"]);
        echo json_encode($checkCategoria);

    }
    public function get_equipo_edit () {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $checkFooter = $this -> Equipo -> findById( $this -> params -> data["id"]);
        echo json_encode($checkFooter);
    }
    public function get_novedades() {
        $this -> layout = "ajax";
        $this -> render("/Elements/save");
        $novedades = $this -> Novedade -> findById( $this -> params -> data["id"]);
        echo json_encode($novedades, JSON_FORCE_OBJECT);


    }
}
