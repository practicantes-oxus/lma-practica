<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class HomeController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session', );
    public $uses = array('Home','Inicio','PalabrasMenu','Palabra', 'Footer');

    public function index() {
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $checkPalabras2 = $this -> Palabra -> findById (1);
        $checkInicio = $this -> Inicio -> findById (1);
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkPalabras2", $checkPalabras2 );
        $this -> set( "checkInicio", $checkInicio );
    }
    public function change() {
        $this->layout = "ajax";
        $continue = str_replace($this->base, "", $this->params->query["continue"]);
        $_SESSION['lang'] = $this->params->named["lang"];
        $this->redirect($continue);
    }
    public function get_footer () {
        $this -> layout = "ajax";
        return $this -> Footer -> find("all");
    }
    public function send() {
        $data = $this->params->data;
        $form = $this->params->form["cv"];

        $Email = new CakeEmail("smtp");
        $Email->to("gonzalo@boycot.cl");
        //$Email->bcc("EMAIL");
        $Email->subject('CV Web');
        $content = "La siguiente persona realizo envio un cv a través del sitio web.<br /><br />";
        $content .= "<b>Nombre:</b> " . $data["first_name"] . "<br />";
        $content .= "<b>Email:</b> " . $data["email"] . "<br />";

        if(!empty($form["name"])) {
            $Email -> attachments(array($form["name"] => array('file' => $form["tmp_name"], 'mimetype' => $form["type"], 'contentId' => 'cv')));
        }
        $Email->send($content);

        $save = array();
        $save["cv"] = $form["name"];
        $save["first_name"] = $data["first_name"];
        $save["email"] = $data["email"];



        $this->Session->setFlash("Mensaje enviado correctamente", "default", array("class" => "col-xs-12 alert alert-success text-center fadeIn animated"), "Home");
        $this->redirect(array("controller" => "Home", "action" => "index"));
    }

}