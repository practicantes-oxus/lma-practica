<?php
App::uses('AppController', 'Controller');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class NovedadesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Home','PalabrasMenu','Palabra','Novedade');

    public function index() {
        $checkNovedade = $this -> Novedade -> find ("all");
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $checkPalabras2 = $this -> Palabra -> findById (1);
        $this -> set( "checkNovedade", $checkNovedade );
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkPalabras2", $checkPalabras2 );

    }
    public function footer() {
        return $this -> Footer -> find("all");
    }
    public function interior() {
        $checkNovedade = $this ->Novedade->findById( $this -> params -> id );
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $checkPalabras2 = $this -> Palabra -> findById (1);
        $this -> set( "checkPalabras2", $checkPalabras2 );
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this->set("checkNovedade", $checkNovedade);
    }
    public function change() {
        $this->layout = "ajax";
        $continue = str_replace($this->base, "", $this->params->query["continue"]);
        $this->Cookie->write("lang", $this->params->named["lang"], false);
        $this->redirect($continue);
    }

}