<?php
App::uses('AppController', 'Controller');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class PracticaController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Home','PalabrasMenu','AreaDePractica');

    public function index() {
        $checkArea = $this -> AreaDePractica -> findById (1);
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkArea", $checkArea );
    }

}