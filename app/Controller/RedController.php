<?php
App::uses('AppController', 'Controller');
/**
 * Home Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property RequestHandlerComponent $RequestHandler
 * @property SessionComponent $Session
 */
class RedController extends AppController {

   /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler', 'Session');
    public $uses = array('Home','PalabrasMenu','Redes');

    public function index() {
        $checkArea = $this -> Redes -> findById (1);
        $checkPalabras1 = $this -> PalabrasMenu -> findById (1);
        //pr($checkArea);die;
        $this -> set( "checkPalabras1", $checkPalabras1 );
        $this -> set( "checkArea", $checkArea );
    }

}