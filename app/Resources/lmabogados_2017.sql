-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql.lmabogados.cl
-- Tiempo de generación: 18-10-2017 a las 06:46:46
-- Versión del servidor: 5.6.34-log
-- Versión de PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `boycot_abogados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_de_practicas`
--

CREATE TABLE `area_de_practicas` (
  `id` int(11) NOT NULL,
  `titulo_area_es` text NOT NULL,
  `area_descrip_es` text NOT NULL,
  `titulo_area_en` text NOT NULL,
  `area_descrip_en` text NOT NULL,
  `titulo_area_pt` text NOT NULL,
  `area_descrip_pt` text NOT NULL,
  `t1_es` text NOT NULL,
  `t1_des_es` text NOT NULL,
  `t1_en` text NOT NULL,
  `t1_des_en` text NOT NULL,
  `t1_pt` text NOT NULL,
  `t1_des_pt` text NOT NULL,
  `t2_es` text NOT NULL,
  `t2_des_es` text NOT NULL,
  `t2_en` text NOT NULL,
  `t2_des_en` text NOT NULL,
  `t2_pt` text NOT NULL,
  `t2_des_pt` text NOT NULL,
  `t3_es` text NOT NULL,
  `t3_des_es` text NOT NULL,
  `t3_en` text NOT NULL,
  `t3_des_en` text NOT NULL,
  `t3_pt` text NOT NULL,
  `t3_des_pt` text NOT NULL,
  `t4_es` text NOT NULL,
  `t4_des_es` text NOT NULL,
  `t4_en` text NOT NULL,
  `t4_des_en` text NOT NULL,
  `t4_pt` text NOT NULL,
  `t4_des_pt` text NOT NULL,
  `t5_es` text NOT NULL,
  `t5_des_es` text NOT NULL,
  `t5_en` text NOT NULL,
  `t5_des_en` text NOT NULL,
  `t5_pt` text NOT NULL,
  `t5_des_pt` text NOT NULL,
  `t6_es` text NOT NULL,
  `t6_des_es` text NOT NULL,
  `t6_en` text NOT NULL,
  `t6_des_en` text NOT NULL,
  `t6_pt` text NOT NULL,
  `t6_des_pt` text NOT NULL,
  `t7_es` text NOT NULL,
  `t7_des_es` text NOT NULL,
  `t7_en` text NOT NULL,
  `t7_des_en` text NOT NULL,
  `t7_pt` text NOT NULL,
  `t7_des_pt` text NOT NULL,
  `t8_es` text NOT NULL,
  `t8_des_es` text NOT NULL,
  `t8_en` text NOT NULL,
  `t8_des_en` text NOT NULL,
  `t8_pt` text NOT NULL,
  `t8_des_pt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area_de_practicas`
--

INSERT INTO `area_de_practicas` (`id`, `titulo_area_es`, `area_descrip_es`, `titulo_area_en`, `area_descrip_en`, `titulo_area_pt`, `area_descrip_pt`, `t1_es`, `t1_des_es`, `t1_en`, `t1_des_en`, `t1_pt`, `t1_des_pt`, `t2_es`, `t2_des_es`, `t2_en`, `t2_des_en`, `t2_pt`, `t2_des_pt`, `t3_es`, `t3_des_es`, `t3_en`, `t3_des_en`, `t3_pt`, `t3_des_pt`, `t4_es`, `t4_des_es`, `t4_en`, `t4_des_en`, `t4_pt`, `t4_des_pt`, `t5_es`, `t5_des_es`, `t5_en`, `t5_des_en`, `t5_pt`, `t5_des_pt`, `t6_es`, `t6_des_es`, `t6_en`, `t6_des_en`, `t6_pt`, `t6_des_pt`, `t7_es`, `t7_des_es`, `t7_en`, `t7_des_en`, `t7_pt`, `t7_des_pt`, `t8_es`, `t8_des_es`, `t8_en`, `t8_des_en`, `t8_pt`, `t8_des_pt`) VALUES
(1, 'ÁREAS DE PRÁCTICA ', '<p>LMA está capacitada para brindar asesoría en todos los ámbitos legales relevantes para emprender negocios en Chile, habiendo desarrollado las competencias necesarias para poder aconsejar y defender eficazmente los intereses de nuestros clientes. Ofrecemos asesoría legal y servicios en las siguientes áreas:</p>', 'PRACTICE AREAS ', '<p>LMA is in a position to provide all the needed legal services required to entrepreneur in Chile having developed the capabilities to advice and effectively defend the interest of our clients. We offer legal advisement and services on the foregoing areas:</p>', 'ÁREAS DE ATUAÇÃO.  ', '<p>LMA possui profissionais capacitados para entregar assessoria em todas as áreas jurídicas relevantes para fazer negócios no Chile, desenvolvendo as competências necessárias para orientar e defender os interesses de nossos clientes de forma eficaz. Oferecemos consultoria e assessoria  jurídica nas seguintes áreas:</p>', 'Litigación', '<p>Defensa de los intereses de empresas en materias civiles, comerciales, laborales, ley de protección al consumidor, y toda clase de litigios originados en el desarrollo de sus actividades.</p>', 'Litigation', '<p>Business defense  of companies interests in civil, commercial, labor, Consumer Protection rights and all types of litigation in the ordinary course of its activities.</p>', ' Litígio', '<p>Defesa dos interesses das empresas em matéria civil, comercial, trabalhista,  Lei de Defesa do Consumidor, e todos os tipos de processos judiciais no curso normal de suas atividades.</p>', 'Derecho constitucional y administrativo', '<p>Defensa de los derechos e intereses de la empresa ante la administración del Estado.</p>', 'Arbitration, Mediation and Alternative Dispute Resolution', '<p>Litigation before arbitrators and extrajudicial solutions.</p>', 'Direito Constitucional e Administrativo', '<p>Proteção dos direitos e interesses da empresa frente a ações do Estado.</p>', 'Derecho inmobiliario y contratos de construcción', '<p>Asesoría legal en todas las etapas del desarrollo de un proyecto inmobiliario y/o de construcción.</p>', 'Constitutional and Administrative Law', '<p>Protecting the rights and interests of the company  in litigations with  the government  administration, both from the faze of the administrative and judicial view, and  legal defense.</p>', 'Direito Imobiliários e Contratos de Construção', '<p>Assessoria jurídica em todas as fases de desenvolvimento de um projeto imobiliário e/ou de construção.</p>', 'Derecho corporativo', '<p>Asesoría en la estructuración legal de las empresas, y cumplimiento de las disposiciones legales, contractuales, reglamentarias y estatutarias que las rigen.</p>', 'Labor and Social Security', '<p>Legal advice on  internal public relations of the company with its employees, both from the perspective of individual rights and the collective labor laws.</p>', 'Direito Corporativo', '<p>Assessoria sobre a estrutura legal das empresas e cumprimento das disposições legais, contratuais, regulamentares e estatutárias que as regem.</p>', 'Arbitraje, mediación y solución alternativa de conflictos', '<p>Litigación ante jueces árbitros y búsqueda de soluciones extrajudiciales.</p>', 'Real Estate and construction contracts', '<p>Legal advice in all phases of real estate project development and / or construction both in the public or private sector.</p>', 'Arbitragem, Mediação e Resolução Alternativa de Litígios', '<p>Contencioso perante árbitros e juízes e busca de soluções extrajudiciais.</p>', 'Derecho laboral y de seguridad social', '<p>Asesoría en las relaciones internas de la empresa con sus trabajadores, tanto desde la perspectiva del derecho individual como del derecho colectivo del trabajo.</p>', 'Civil and Commercial Contracts', '<p>Asesoría en las relaciones internas de la empresa con sus trabajadores, tanto desde la perspectiva del derecho individual como del derecho colectivo del trabajo.</p>', 'Direito do Trabalho e Previdenciário. ', '<p>Assessoria nas relações internas da empresa aos seus empregados, tanto do ponto de vista dos direitos individuais e do direito do trabalho coletivo.</p>', 'Contratación civil y comercial', '<p>Asesoría en la celebración de todo contrato relacionado al normal desarrollo de las actividades de la empresa.</p>', 'Corporate Law', '<p>Advice on the legal structure of the companies, in compliance with legal, contractual, regulatory and statutory conditions  in existence.</p>', 'Contrato Civil e Comercial', '<p>Assessoria  sobre a realização de contratos associados ao desenvolvimento dos negócios da empresa.</p>', 'Derecho tributario', '<p>Análisis y asesoría sobre el marco regulatorio tributario para las empresas , en todos sus niveles, con&nbsp;especial experiencia en tributación internacional.</p>', 'Taxation Law', '<p>Analysis and advisement on the legal regulatory frame of Taxes for companies, with an&nbsp;special expertise in International Taxation.</p>', 'Direito Fiscal', '<div class=\"tw-swapa\"><div class=\"_wYb\"></div></div><div class=\"_Ejb\"><div id=\"tw-target\"><div class=\"tw-ta-container tw-nfl\" id=\"tw-target-text-container\"><pre class=\"tw-data-text tw-ta tw-text-small\" id=\"tw-target-text\" style=\"height: 120px; text-align: left;\" dir=\"ltr\" data-fulltext=\"\" data-placeholder=\"Traducción\"><span lang=\"pt\"><div class=\"tw-swapa\"><div class=\"_wYb\"></div></div><div class=\"_Ejb\"><div id=\"tw-target\"><div class=\"tw-ta-container tw-nfl\" id=\"tw-target-text-container\"><pre class=\"tw-data-text tw-ta tw-text-small\" id=\"tw-target-text\" style=\"height: 120px; text-align: left;\" dir=\"ltr\" data-fulltext=\"\" data-placeholder=\"Traducción\"><span lang=\"pt\">Análise e aconselhamento sobre o quadro regulamentar fiscal para as empresas em todos os níveis, com conhecimento especializado em tributação internacional</span></pre></div></div></div></span></pre></div></div></div>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `titulo_es` varchar(255) NOT NULL,
  `titulo_en` varchar(255) NOT NULL,
  `titulo_pt` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `titulo_es`, `titulo_en`, `titulo_pt`, `active`, `created`) VALUES
(1, 'Socios', 'Partners', 'Socios', 1, '2015-03-11 19:10:20'),
(2, 'Asociados Séniors', ' Seniors Associates', 'Associados Sêniors', 1, '2015-03-17 17:15:58'),
(3, 'Asociados', 'Associates', 'Associados', 1, '2015-03-17 17:17:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `direccion_es` text NOT NULL,
  `direccion_en` varchar(400) NOT NULL,
  `direccion_pt` varchar(400) NOT NULL,
  `descrip_es` varchar(400) NOT NULL,
  `descrip_en` varchar(400) NOT NULL,
  `descrip_pt` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `direccion_es`, `direccion_en`, `direccion_pt`, `descrip_es`, `descrip_en`, `descrip_pt`) VALUES
(1, 'CONTACTO', 'CONTACT', 'CONTATO', '<span style=\"font-weight: bold;\">Avenida&nbsp;Isidora Goyenechea&nbsp;3120 piso 16 (Metro El Golf)&nbsp;</span><div>Santiago de Chile. Comuna de Las Condes.&nbsp;</div><div>Mail: info@lmabogados.cl</div><div>Tel: (56 2) 2799 28 00&nbsp;</div>', '<span style=\"font-weight: bold;\">Isidora Goyenechea Avenue, N° 3120, 16th floor (El Golf Subway Station)</span><div>Santiago of Chile. Las Condes.&nbsp;</div><div>Mail: info@lmabogados.cl</div><div>Phone: (56 2) 2799 28 00&nbsp;</div>', '<div>Endereço<b></b><br></div><b>Avenida </b><span style=\"font-weight: bold;\">Isidora Goyenechea&nbsp;</span><b> 3120, 16° andar (Metro El Golf)</b><div>Bairro de Las Condes, Santiago, Chile.</div><div>Mail: info@lmabogados.cl</div><div>Tel: (56 2) 2799 28 00</div>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `titulo_es` varchar(255) NOT NULL,
  `titulo_en` varchar(255) NOT NULL,
  `titulo_pt` varchar(255) NOT NULL,
  `descripcion_es` longtext NOT NULL,
  `descripcion_en` longtext NOT NULL,
  `descripcion_pt` longtext NOT NULL,
  `categoria` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `cv` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `titulo_es`, `titulo_en`, `titulo_pt`, `descripcion_es`, `descripcion_en`, `descripcion_pt`, `categoria`, `imagen`, `cv`, `active`, `created`) VALUES
(2, 'José Joaquin Lagos Velasco', 'José Joaquin Lagos Velasco', 'José Joaquin Lagos Velasco', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Abogado, Universidad Diego Portales. Actualmente está cursando el Magíster en derecho privado, Universidad de Chile. Diplomado en Recurso de Protección, Universidad Diego Portales y Diplomado en derecho Tributario, Universidad de Chile.</span></p><p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Anteriormente, socio en Espina, Zepeda, Rodríguez y Tavolari Abogados y abogado en Cariola y Cía. En el ámbito académico, el señor Lagos fue Director de la Escuela de Derecho de la Universidad Mayor y profesor titular de la cátedra de derecho civil.</span><br></p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Litigios civiles y comerciales, juicios arbitriales y protección al consumidor.</p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español, Francés e Inglés.</p>\r\n\r\n<p style=\"text-align: justify;\">jlagos@lmabogados.cl</p>', '<p><span style=\"line-height: 1.42857143;\">Lawyer – University Diego Portales, Master’s studies in private Law –University of Chile – Postgraduate studies in Constitutional Injunctions – University Diego Portales, Postgraduate Zepeda Rodriguez and Tavolari Lawyers and lawyers at Cariola y Cia. Mr. Lagos has been Director of the Law School of Mayor University and professor of Civil Law.</span><br></p>\r\n<p>Practice Areas:</p>\r\n<p>Litigation practice in private law, commercial law and, consumers law.</p><p>Languages:</p>\r\n<p>Spanish, French and Englis.</p>', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Advogado, Universidade Diego Portales. Atualmente cursando Mestrado em Direito Privado na Universidade do Chile. Especialização em Recurso de Proteção, Universidade Diego Portales e Especialização em Direito Tributário, Universidade de Chile. Anteriormente, sócio em Espina, Zepeda, Rodríguez y Tavolari Advogados e advogado em Cariola y Cía. Na área acadêmica, foi Diretor da Escola de Direito da Universidade Mayor e professor titular da cadeira de Direito Civil.</span><br></p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Litígios civis e comerciais, juízos arbitrais e proteção ao consumidor.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol, Francês e Inglês.</p>\r\n<p style=\"text-align: justify;\">jlagos@lmabogados.cl</p>', 1, '1430248803.jpg', '', 1, '2015-03-17 18:40:42'),
(3, 'Kenneth Maclean Luengo', 'Kenneth Maclean Luengo', 'Kenneth Maclean Luengo', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Abogado, Pontificia Universidad Católica de Chile. Magíster en derecho de los negocios, Universidad Adolfo Ibáñez y Diplomado en economía y finanzas, Universidad Gabriela Mistral. Anteriormente en Cariola y Cia. Y en Viñes y Asociados. El señor Maclean ejerce activamente su profesión con compañías multinacionales de los sectores agroindustrial, industria, minería, tecnológico y “Retail”.</span><br></p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Derecho Laboral colectivo e individual, derecho comercial, societario, económico y contratación moderna. Litigios comerciales, laborales y arbitrales.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español e inglés.</p>\r\n\r\n<p style=\"text-align: justify;\">kmaclean@lmabogados.cl</p>\r\n', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Lawyer – Pontifical Catholic University of Chile. Master in Business Law – Universidad Adolfo Ibáñez. Postgraduate studies in Economics and Finance – University Gabriela Mistral. Formerly at Cariola y Cia and in Viñes Asociados. Mr. Maclean has an active practice with transnational companies in the areas of Agro Industrial, Mining, Industrial, Technology and Retail.</span><br></p>\r\n<p style=\"text-align: justify; \">Practice Areas:</p>\r\n<p style=\"text-align: justify;\">Advisor and litigation practices in labor law, public law litigation, corporate matters, contracts, development and, real estate.</p><div style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Languages:</span></div><p></p>\r\n<p style=\"text-align: justify;\">Spanish and English.</p>\r\n', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Advogado, Pontifícia Universidade Católica de Chile. Mestrado em Direito dos Negócios, Universidade Adolfo Ibañez e Especialização em Economia e Financias, Universidade Gabriela Mistral. Anteriormente, em Cariola y Cía e em Viñes y Asociados. Exerceu ativamente sua profissão em companhias multinacionais dos setores agroindustrial, mineiro, industrial, tecnológico e retail.</span><br></p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Direito Trabalhista, Comercial, Societário, Econômico e Contratação Moderna. Litigios Comerciais, Trabalhistas e arbitrais.</p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol e Inglês.</p>\r\n\r\n<p style=\"text-align: justify;\">kmaclean@lmabogados.cl</p>', 1, '1428670533.jpg', '', 1, '2015-03-17 18:37:22'),
(4, 'Raimundo Allende Zañartu', 'Raimundo Allende Zañartu', 'Raimundo Allende Zañartu', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Abogado, Universidad de Chile. Anteriormente, 2014-2008 se desempeño como abogado jefe de SMU S.A. y con igual posición en Walmart Chile S.A. En Forma previa al sector Retail, por más de 12 años fue abogado de la empresa Telefónica Chile y socio en Carey &amp; Allende Abogados.</span><br></p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de práctica:</p><div style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Derecho civil y comercial, asuntos corporativos, contractuales, inmobiliarios y de telecomunicaciones.</span></div><p></p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español e Inglés.</p>\r\n\r\n<p style=\"text-align: justify;\">rellende@lmabogados.cl</p>', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Lawyer - University Of Chile, postgraduate studies in commercial and corporate law. More tan 25 years of experience advising real estate, telecommunications and retail companies in the corporate and commercial lawfields. Formerly was partner at Carey &amp; Allende Lawyers ant lawyer at the Teléfonica Chile Group.</span><br></p>\r\n<p style=\"text-align: justify; \">Practice Areas:</p>\r\n<p style=\"text-align: justify;\">Advisor in real estate, telecommunications and retail companies in the corporate and commercial lawfields.</p>\r\n<p style=\"text-align: justify; \">Languages:</p>\r\n<p style=\"text-align: justify;\">Spanish and English.</p>\r\n', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Advogado, Universidade de Chile. Entre os anos de 2008 e 2014, exerceu como advogado Chefe na SMU S.A., e como Advogado Chefe em Walmart Chile S.A. Anteriormente, exerceu por 12 anos como advogado da Telefonica Chile e foi sócio em Carey &amp; Allende Advogados.</span><br></p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Direito Civil e Comercial, assuntos corporativos, contratuais, imobiliários e de telecomunicações.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify; \">Espanhol e Inglês.</p>\r\n<p style=\"text-align: justify; \">rellende@lmabogados.cl</p>', 1, '1428435592.jpg', '', 1, '2015-03-17 18:27:53'),
(100, 'Pedro García Ossandon', 'Pedro García Ossandon', 'Pedro García Ossandon', '<p>Abogado Universidad Católica del Norte en Antofagasta. Magister en Derecho Universidad de Chile, mención en derecho público.Áreas de practica: Asesoría y litigación en materias de derecho laboral, comercial, público&nbsp; y regulatorio.Idiomas: español.<br></p>', '<p>&nbsp;Lawyer – Catholic University of the North at Antofagasta, LLM - University of Chile majoring in public law.Practice Areas: Advisor and litigation practices in labor law, commercial law, public law and&nbsp; regulatory law. Languages: spanish.<br></p>', '<p>Advogado,&nbsp; Universidade Católica do Norte em Antofagasta. Mestrado em Direito , Universidade de Chile, com especialização em direito público. &nbsp;Áreas de Atuação: assessoria e litigância em matérias de direito trabalhista, comercial, público&nbsp; e regulatório.&nbsp;Idiomas: espanhol.<br></p>', 1, '1459864533.png', '1459864533.png', 1, '2016-04-05 10:55:33'),
(110, 'Catalina Parker COLL', 'Catalina Parker COLL', 'Catalina Parker COLL', '<p style=\"text-align: justify; \">Abogada, Pontificia Universidad Católica. Tiene un diplomado en derecho de recursos naturales y energía en la misma Universidad. Antes de integrarse a LM Abogados, trabajó en la Contraloría General de la República (2011-2014), y previo a ello en una Fundación para el financiamiento de educación superior. (2010-2011).</p>\r\n<p style=\"text-align: justify;\">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Materias corporativas, derecho administrativo, y asuntos regulatorios.</p>\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify; \">Español e Inglés.</p>', '<p>Lawyer, Pontificia Universidad Católica University. She has a Diploma on natural resources and energy law, at the same University. Formerly she worked at Contraloría General de la República (2011-2014), and on a Foundation of education (2010-2011).</p>\r\n<p>Practice Areas:</p>\r\n<p>Corporate Matters, administrative law, and regulatory matters.</p>\r\n<p>Languages:</p>\r\n<p>Spanish and English.</p>', '<p style=\"text-align: justify; \">Advogada, Pontifícia Universidade Católica do Chile. Especialização em Direito Energético e Recursos Naturais, também na Universidade Católica. Exerceu na Controladoría General de la República (2011 – 2014), e em uma Fundação educacional (2010 – 2011).</p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify; \">Direito Corporativo, Administrativo e Regulatório.</p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol e Inglês.</p>', 2, '1426631782.jpg', '', 1, '2015-03-17 19:04:10'),
(115, 'Javiera Días Schmidt', 'Javiera Días Schmidt', 'Javiera Días Schmidt', '<p style=\"text-align: justify;\"><span style=\"line-height: 1.42857143;\">Abogado, Universidad Gabriela Mistral. Actualmente secretaria general y presidenta del comité de autoevaluación del centro de formación técnica ENAC caritas – Chile. Anteriormente desempeño diferentes posiciones ejecutivas en las universidades Gabriela Mistral y Mayor de Chile. 2007-2002 secretaria general de la Cámara chileno-luxemburguesa (2002-1997).</span><br></p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Sector educación superior, organizaciones sin fines de lucro, materias corporativas, contractuales e inmobiliarias.</p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español, Francés e Inglés.</p>\r\n\r\n<p style=\"text-align: justify;\">jdiaz@lmabogados.cl</p>\r\n', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Lawyer, Gabriela Mistral University. Legal counsel and President of the Committee of Self-evaluation for the centro de formación Técnica ENAC Cáritas Chile. Formerly Mrs. Diaz hold different executive positions on the Gabriela Mistral and Universidad Mayor Universities (2002-2007) and was the general secretary of the Chilean-Luxembourg Chamber of Commerce (1997-2002).</span><br></p>\r\n<p style=\"text-align: justify; \">Practice Areas:</p>\r\n<p style=\"text-align: justify;\">Education, Nonprofit Organizations, Corporate Matters, Contracts and Real Estate.</p>\r\n<p style=\"text-align: justify; \">Lenguages:</p>\r\n<p style=\"text-align: justify;\">Spanish, French and English.</p>\r\n', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Advogada, Universidade Gabriela Mistral. Atualmente Secretária Geral e Presidente do Comitê de Auto Avaliação do Centro de Formação Técnica ENAC Cáritas – Chile. Anteriormente desempenhou diferentes posições executivas nas Universidades Gabriela Mistral e Mayor, de Chile.</span><br></p>\r\n<p style=\"text-align: justify;\">Entre os anos de 1997 a 2002, foi Secretaria Geral da Câmara Chileno-Luxemburguesa.</p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Educação Superior, ONGs, matérias corporativas, contratuais e imobiliárias.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol, Francês e Inglês.</p>\r\n\r\n<p style=\"text-align: justify;\">jdiaz@lmabogados.cl</p>', 2, '1428670550.jpg', '', 1, '2015-03-11 20:22:32'),
(120, 'José A. Castro González', 'José A. Castro González', 'José A. Castro González', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Abogado, Universidad Las Condes, MBA, Adolfo Ibáñez Management school, Miami, Fl; MBA, Universidad de DEUSTO, España; Diplomado en Gestión tributaria, Universidad Adolfo Ibáñez y diplomado en tributación, Universidad de Chile. Anteriormente, Fiscal y secretario general de Universidad Mayor, Chile y abogado de la dirección de asesoría jurídica de la I. Municipalidad de Santiago, en el ámbito académico, el señor Castro fue rector de la Universidad Innova College Business School, Miami,  Florida y profesor titular de la cátedra de derecho inmobiliario en Universidad Mayor.</span><br></p>\r\n<p style=\"text-align: justify; \">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Derecho comercial y municipal, sector educación, tecnología y organizaciones sin fines de lucro, inmobiliario y contratación.</p>\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español e Inglés.</p>\r\n<p style=\"text-align: justify;\">jcastro@lmabogados.cl</p>', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Lawyer – University Las Condes, MBA – Adolfo Ibañez Management School, MBA – Deusto University, Postgraduate degree in Tax Planning and Management, University Adolfo Ibáñez. Formerly head of inside legal counsel of Mayor University and lawyer of the Santiago district. Mr. Castro has been CEO of the Innova College Business School, Miami Florida and professor of Real Estate law.</span><br></p>\r\n<p style=\"text-align: justify; \">Practice Areas:</p>\r\n<p style=\"text-align: justify;\">Advisor in Commercial law, Education, Technology, Non Profit Organization, and Real State.</p>\r\n<p style=\"text-align: justify; \">Languages:</p>\r\n<p style=\"text-align: justify;\">Spanish and English.</p>\r\n', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Advogado, Universidade Las Condes. MBA, Adolfo Ibañez Management School, Miami, Fl. MBA, Universidade de Deusto, Espanha. Especialização em Gestão Tributária, Universidade Adolfo Ibañez e em Tributação, Universidade do Chile. Também, é advogado da Prefeitura de Santiago. Na área docente, foi Reitor da Universidade Innova College Business School, Miami, Florida e professor titular da cadeira de Direito Imobiliário da Universidade Mayor.</span><br></p>\r\n\r\n<p style=\"text-align: justify; \">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Direito Comercial e Municipal, Setor Educação, Tecnologi, ONGs, Imobiliário e Contratação.</p>\r\n\r\n<p style=\"text-align: justify; \">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol e Inglês.</p>', 2, '1427213112.jpg', '', 1, '2015-03-11 20:08:19'),
(125, 'Jose Miguel Rosas Chuaqui', 'Jose Miguel Rosas Chuaqui', 'Jose Miguel Rosas Chuaqui', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Abogado, Universidad de Chile. Actualmente cursando magíster en derecho – mención derecho privado, Universidad de Chile. Anteriormente, en 2003- 1999 se desempeñó como procurador en Cariola, Diez, Peres Cotapos y Cía.</span><br></p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de práctica:</p>\r\n<p style=\"text-align: justify;\">Derecho civil y procesal: Abogado litigante en asuntos de responsabilidad civil, contractual y extracontractual, en sede civil, arbitral, tribunales superiores de justicia y policía local.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Español.</p>\r\n\r\n<p style=\"text-align: justify;\">jrosas@lmabogados.cl</p>', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Lawyer – University of Chile. Master’s studies in Private Law – University of Chile -. Fomerly at Cariola, Diez, Pérez Cotapos y Cia. With Lagos Maclean from 2005.</span><br></p>\r\n<p style=\"text-align: justify; \">Practice Areas:</p>\r\n<p style=\"text-align: justify;\">Litigation practice in private law, commercial law and, consumers law.</p>\r\n<p style=\"text-align: justify; \">Languages:</p>\r\n<p style=\"text-align: justify; \">Spanish.</p>', '<p style=\"text-align: justify; \"><span style=\"line-height: 1.42857143;\">Advogado, Universidade de Chile. Atualmente cursando Mestrado em Direito, com especialização em Direito Privado, na Universidade de Chile. Anteriormente, 1999-2003, exerceu na escritório Cariola, Perez, Cotapos y Cía.</span><br></p>\r\n\r\n<p style=\"text-align: justify;\">Áreas de Atuação:</p>\r\n<p style=\"text-align: justify;\">Direito Civil e Processual Civil. Advogado litigante em matérias de responsabilidade civil, contratual e extracontratual, em sede Civil, Arbitral, Tribunais Superiores de Justiça.</p>\r\n\r\n<p style=\"text-align: justify;\">Idiomas:</p>\r\n<p style=\"text-align: justify;\">Espanhol.</p>\r\n\r\n<p style=\"text-align: justify;\">jrosas@lmabogados.cl</p>', 2, '1427213151.jpg', '', 1, '2015-03-17 18:33:56'),
(130, 'Silvia Morales Cáceres', 'Silvia Morales Cáceres', 'Silvia Morales Cáceres', '<p>Abogada, Pontificia Universidad Católica. Tiene un diplomado en relaciones laborales en la empresa por la Universidad de Chile y un diplomado en gestión de equipos exitosos del programa Eclass. Antes de integrarse a LM Abogados, trabajó en Constructora 3L S.A. (2014-2016), y previo a ello en Cobranzas Judiciales SpA. (2014) y Yavar &amp; Silva (2012-2013).Áreas de práctica: Derecho laboral individual y colectivo y Litigación. Idiomas:Español e Inglés.</p><p>smorales@lmabogados.cl<br></p>', '<p>Lawyer, Pontifical Catholic University of Chile. She has a Diploma on labour relations, at University of Chile and . Formerly she worked at Contraloría General de la República (2011-2014), and on a Foundation of education (2010-2011).Practice Areas: Individual and collective labour Law and litigation.</p><p>smorales@lmabogados.cl<br></p>', '<p>Advogado, Universidade Católica do Chile. Possui especializaçao em relaçoes trabalhistas da empresa pela Universidade de Chile e especializaçao em gestao de equipes exitosas pelo programa Eclass. Antes de integrar-se a LM Abogados, trabalhou na Construtora 3L S.A. (2014-2016), e anteriormente em Cobranzas Judiciales SpA. (2014) e Yavar $ Silva (2012-2013).Área de atuação: matérias corporativas, direito administrativo e assuntos regulatórios.Idiomas: Espanhol e Inglês.</p><p>smorales@lmabogados.cl</p>', 2, '1503951299.jpg', '', 1, '2016-04-05 11:17:31'),
(135, 'Constanza Galaz Pimiento', 'Constanza Galaz Pimiento', 'Constanza Galaz Pimiento', '\r\n\r\n\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 18.0px; font: 16.0px \'Times New Roman\'; color: #1f497d; -webkit-text-stroke: #1f497d; background-color: #ffffff}\r\np.p2 {margin: 0.0px 0.0px 10.0px 0.0px; line-height: 16.0px; font: 14.0px \'Helvetica Light\'; color: #424a4d; -webkit-text-stroke: #424a4d; background-color: #ffffff; min-height: 17.0px}\r\np.p3 {margin: 0.0px 0.0px 10.0px 0.0px; line-height: 16.0px; font: 14.0px \'Helvetica Light\'; color: #424a4d; -webkit-text-stroke: #424a4d}\r\np.p4 {margin: 0.0px 0.0px 10.0px 0.0px; line-height: 16.0px; font: 13.3px Tahoma; color: #1155cc; -webkit-text-stroke: #1155cc; background-color: #ffffff}\r\nspan.s1 {font-kerning: none}\r\n</style>\r\n\r\n\r\n<p class=\"p1\"><span class=\"s1\">Abogada Pontificia Universidad Católica de Chile, con certificado académico de especialidad en Derecho del Trabajo y Seguridad Social.</span></p>\r\n<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>\r\n<p class=\"p1\"><span class=\"s1\">Ayudante de cátedra de Derecho del Trabajo y Seguridad Social en la Pontificia Universidad Católica de Chile desde el 2016.</span></p>\r\n<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>\r\n<p class=\"p1\"><span class=\"s1\">Áreas de práctica: Derecho Laboral y litigación.</span></p>\r\n<p class=\"p1\"><span class=\"s1\">&nbsp;</span></p>\r\n<p class=\"p2\"><span class=\"s1\"></span><br></p>\r\n<p class=\"p1\"><span class=\"s1\">Lenguajes: Español e Inglés.</span></p>\r\n<p class=\"p3\"><span class=\"s1\"><br>\r\n</span></p>\r\n<p class=\"p4\"><span class=\"s1\"><a href=\"mailto:cgalaz@lmabogados.cl\">cgalaz@lmabogados.cl</a></span></p>', '<p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">Lawyer at Pontificia Universidad Católica de Chile,&nbsp;</span><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">with a&nbsp;</span><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">certificate in Labour Law and Social Security.<u></u><u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\"><u></u>&nbsp;<u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">Assitant profesor of Labour Law and Social Security at Pontificia Universidad Católica de Chile since 2016.<u></u><u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\"><u></u>&nbsp;<u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">Significant practice areas include: Labour law and litigation.<u></u><u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\"><u></u>&nbsp;<u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">Languages<u></u><u></u></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">Spanish and English.</span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\"><br></span></p><p class=\"MsoNormal\" style=\"margin-right: 0cm; margin-bottom: 0px; margin-left: 0cm; font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: rgb(34, 34, 34);\"><span lang=\"EN-US\" style=\"color: rgb(31, 73, 125);\">cgalaz@lmabogados.cl</span></p>', '<p><span style=\"color: rgb(31, 73, 125); font-family: &quot;Times New Roman&quot;, serif; font-size: 16px;\">cgalaz@lmabogados.cl</span><br></p>', 3, '1503952113.jpg', '', 1, '2016-04-05 11:12:52'),
(140, 'Claudia Quappe Vatter', 'Claudia Quappe Vatter', 'Claudia Quappe Vatter', '<p>Abogada, Pontificia Universidad Católica de Chile.Área de Práctica: Litigios.Idiomas: Español y Aleman.</p><p>cquappe@lmabogados.cl<br></p>', '<p>Attorney at law, Pontifical Catholic University of Chile.Practice Areas: Litigation.Language: spanish and germany.</p><p>cquappe@lmabogados.cl<br></p>', '<p>Advogada, Pontifícia Universidade Católica de Chile.Área de Atuação: Litígios.Idiomas: Espanhol e Alemão.</p><p>cquappe@lmabogados.cl<br></p>', 3, '1459866489.png', '', 1, '2016-04-05 11:13:40'),
(145, 'Matías Ruiz Leiva', 'Matías Ruiz Leiva', 'Matías Ruiz Leiva', '<p>Abogado, Universidad Gabriela Mistral.&nbsp; Anteriormente fue procurador en el Departamento de Derecho Laboral y Policía Local en Lagos Maclean.Áreas de Práctica: Litigios en Policía Local, Ley del Consumidor y Derecho Laboral.Idiomas: Español.</p><p>mruiz@lmabogados.cl<br></p>', '<p>Lawyer,&nbsp; Gabriela Mistral University. Previously, he was in the Department of Labor Law and local police in Lagos Maclean.Practice Areas: Litigation in Municipal Justice, Consumer Law and Labor Law.Language: Spanish.</p><p>mruiz@lmabogados.cl<br></p>', '<p>Advogado, Universidade Gabriela Mistral. Anteriormente, atuou no Departamento de direito trabalhista e de policia local em Lagos Maclean Abogados.Áreas de Atuação: Contencioso na polícia local, Direito do Consumidor e Direito do Trabalho.Idiomas: Espanhol.</p><p>mruiz@lmabogados.cl</p>', 3, '1459865674.png', '', 1, '2016-04-05 11:14:34'),
(150, 'María Belén Kútulas Alvarado', 'María Belén Kútulas Alvarado', 'María Belén Kútulas Alvarado', '<p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Abogada de la Pontificia Universidad Católica de Chile.<u></u><u></u></p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Áreas de Práctica: Derecho Laboral y Litigación.<u></u><u></u></p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Idiomas: Español e Inglés.</p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><br></p><p><a href=\"mailto:bkutulas@lmabogados.cl\" target=\"_blank\" style=\"color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);\">bkutulas@lmabogados.cl</a><br></p>', '<p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Lawyer at Pontificia Universidad Católica de Chile.<u></u><u></u></p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Significant practice areas include: Labour Law and litigation.</p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><br></p><p class=\"MsoNormal\" style=\"margin-bottom: 0px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><a href=\"mailto:bkutulas@lmabogados.cl\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); color: rgb(17, 85, 204); font-size: 12.8px;\">bkutulas@lmabogados.cl</a><br></p>', '<p><a href=\"mailto:bkutulas@lmabogados.cl\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); color: rgb(17, 85, 204); font-family: arial, sans-serif; font-size: 12.8px;\">bkutulas@lmabogados.cl</a><br></p>', 3, '1503952397.jpg', '', 1, '2016-04-05 11:19:29'),
(151, 'Romina Miranda', 'Romina Miranda', 'Romina Miranda', '<p>Abogada Pontificia Universidad Católica de Chile, con certificado académico de especialidad en Derecho del Trabajo y Seguridad Social.</p><p>&nbsp;</p>\r\n<p>Ayudante de cátedra de Derecho del Trabajo y Seguridad Social en la Pontificia Universidad Católica de Chile desde el 2016.</p>\r\n<p>&nbsp;</p>\r\n<p>Áreas de práctica: Derecho Laboral y litigación.</p>\r\n<p>&nbsp;</p>\r\n<p>Lenguajes: Español e Inglés.</p>\r\n<p><a href=\"http://mailto:rmiranda@lmabogados.cl/\" target=\"_blank\">rmiranda@lmabogados.cl</a></p>', 'Lawyer at Pontificia Universidad Católica de Chile,&nbsp;with a&nbsp;certificate in Labour Law and Social Security. &nbsp;<div>Assitant profesor of Labour Law and Social Security at Pontificia Universidad Católica de Chile since 2016.</div><div><br></div><div>Significant practice areas include: Labour law and litigation.\r\n&nbsp;\r\nLanguages\r\nSpanish and English.\r\n?</div><div><br></div><div><a href=\"http://mailto:rmiranda@lmabogados.cl/\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255);\">rmiranda@lmabogados.cl</a><br></div>', '<p><a href=\"http://mailto:rmiranda@lmabogados.cl/\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); font-family: Tahoma; font-size: 13.3px; -webkit-text-stroke-color: rgb(17, 85, 204);\">rmiranda@lmabogados.cl</a><br></p>', 3, '1506605104.jpg', '', 1, '2017-09-28 10:11:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos_des`
--

CREATE TABLE `equipos_des` (
  `id` int(11) NOT NULL,
  `descrip_es` text NOT NULL,
  `descrip_en` text NOT NULL,
  `descrip_pt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos_des`
--

INSERT INTO `equipos_des` (`id`, `descrip_es`, `descrip_en`, `descrip_pt`) VALUES
(1, '<p>Lagos Maclean Abogados está integrado por personas de demostrada competencia y dedicación a su profesión. Nuestro equipo de trabajo desarrolla sus tareas con creatividad, dinamismo y oportunidad al enfrentar los requerimientos de nuestros clientes. Los socios de LMA tienen vasta experiencia en sus campos profesionales, al tiempo que realizan docencia en cátedras relacionadas con las áreas de especialidad de nuestro estudio.</p>', '<p>Lagos Maclean is compose by people of proved competence and dedication to them work. Our team performs them tasks for the clients with creativity, dynamism and opportunity. LMA partners had a vast experience on them working fields alongside with an academic work on the practice areas of our office.</p>', '\r\n\r\n\r\n\r\n\r\n\r\n\r\n<!--[if gte mso 9]><xml>\r\n <o:DocumentProperties>\r\n  <o:Revision>0</o:Revision>\r\n  <o:TotalTime>0</o:TotalTime>\r\n  <o:Pages>1</o:Pages>\r\n  <o:Words>64</o:Words>\r\n  <o:Characters>356</o:Characters>\r\n  <o:Company>Boycot</o:Company>\r\n  <o:Lines>2</o:Lines>\r\n  <o:Paragraphs>1</o:Paragraphs>\r\n  <o:CharactersWithSpaces>419</o:CharactersWithSpaces>\r\n  <o:Version>14.0</o:Version>\r\n </o:DocumentProperties>\r\n <o:OfficeDocumentSettings>\r\n  <o:AllowPNG></o:AllowPNG>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]-->\r\n\r\n<!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves></w:TrackMoves>\r\n  <w:TrackFormatting></w:TrackFormatting>\r\n  <w:HyphenationZone>21</w:HyphenationZone>\r\n  <w:PunctuationKerning></w:PunctuationKerning>\r\n  <w:ValidateAgainstSchemas></w:ValidateAgainstSchemas>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF></w:DoNotPromoteQF>\r\n  <w:LidThemeOther>ES-TRAD</w:LidThemeOther>\r\n  <w:LidThemeAsian>JA</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables></w:BreakWrappedTables>\r\n   <w:SnapToGridInCell></w:SnapToGridInCell>\r\n   <w:WrapTextWithPunct></w:WrapTextWithPunct>\r\n   <w:UseAsianBreakRules></w:UseAsianBreakRules>\r\n   <w:DontGrowAutofit></w:DontGrowAutofit>\r\n   <w:SplitPgBreakAndParaMark></w:SplitPgBreakAndParaMark>\r\n   <w:EnableOpenTypeKerning></w:EnableOpenTypeKerning>\r\n   <w:DontFlipMirrorIndents></w:DontFlipMirrorIndents>\r\n   <w:OverrideTableStyleHps></w:OverrideTableStyleHps>\r\n   <w:UseFELayout></w:UseFELayout>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"></m:mathFont>\r\n   <m:brkBin m:val=\"before\"></m:brkBin>\r\n   <m:brkBinSub m:val=\"&#45;-\"></m:brkBinSub>\r\n   <m:smallFrac m:val=\"off\"></m:smallFrac>\r\n   <m:dispDef></m:dispDef>\r\n   <m:lMargin m:val=\"0\"></m:lMargin>\r\n   <m:rMargin m:val=\"0\"></m:rMargin>\r\n   <m:defJc m:val=\"centerGroup\"></m:defJc>\r\n   <m:wrapIndent m:val=\"1440\"></m:wrapIndent>\r\n   <m:intLim m:val=\"subSup\"></m:intLim>\r\n   <m:naryLim m:val=\"undOvr\"></m:naryLim>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"\r\n  DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"276\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Table Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"></w:LsdException>\r\n </w:LatentStyles>\r\n</xml><![endif]-->\r\n\r\n<!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\ntable.MsoNormalTable\r\n	{mso-style-name:\"Tabla normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n	mso-para-margin:0cm;\r\n	mso-para-margin-bottom:.0001pt;\r\n	mso-pagination:widow-orphan;\r\n	font-size:12.0pt;\r\n	font-family:Cambria;\r\n	mso-ascii-font-family:Cambria;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Cambria;\r\n	mso-hansi-theme-font:minor-latin;}\r\n</style>\r\n<![endif]-->\r\n\r\n\r\n\r\n<!--StartFragment-->\r\n\r\n<p class=\"MsoNormal\"><span lang=\"PT-BR\">Lagos Maclean\r\nAdvogados é composto por pessoas com comprovada competência e dedicação à sua\r\nprofissão. Nossa equipe realiza suas tarefas com criatividade, dinamismo e de\r\nforma oportuna ao momento de enfrentar os requerimentos dos nossos clientes. Os\r\nsócios de LMA possuem uma vasta experiência em suas áreas profissionais, ao\r\nmesmo tempo que desenvolvem a docência em áreas associadas as especializações\r\ndo escritório.<o:p></o:p></span></p>\r\n\r\n<!--EndFragment-->');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `footers`
--

CREATE TABLE `footers` (
  `id` int(11) NOT NULL,
  `href` varchar(500) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `footers`
--

INSERT INTO `footers` (`id`, `href`, `imagen`, `active`, `created`) VALUES
(14, '', '1426605399.png', 1, '2015-03-17 12:16:40'),
(15, '', '1426605419.png', 1, '2015-03-17 12:16:59'),
(16, '', '1426605430.png', 1, '2015-03-17 12:17:10'),
(18, '', '1430249345.jpg', 1, '2015-04-28 15:29:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inicios`
--

CREATE TABLE `inicios` (
  `id` int(11) NOT NULL,
  `titulo1_es` text NOT NULL,
  `nuestro_estudio_es` text NOT NULL,
  `titulo1_en` text NOT NULL,
  `nuestro_estudio_en` text NOT NULL,
  `titulo1_pt` text NOT NULL,
  `nuestro_estudio_pt` text NOT NULL,
  `titulo2_es` varchar(255) NOT NULL,
  `trabajo_bono_es` text NOT NULL,
  `titulo2_en` varchar(255) NOT NULL,
  `trabajo_bono_en` text NOT NULL,
  `titulo2_pt` varchar(255) NOT NULL,
  `trabajo_bono_pt` text NOT NULL,
  `titulo3_es` varchar(255) NOT NULL,
  `postulaciones_es` text NOT NULL,
  `titulo3_en` varchar(255) NOT NULL,
  `postulaciones_en` text NOT NULL,
  `titulo3_pt` varchar(255) NOT NULL,
  `postulaciones_pt` text NOT NULL,
  `text_slider_es` varchar(255) NOT NULL,
  `text_slider_en` varchar(255) NOT NULL,
  `text_slider_pt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inicios`
--

INSERT INTO `inicios` (`id`, `titulo1_es`, `nuestro_estudio_es`, `titulo1_en`, `nuestro_estudio_en`, `titulo1_pt`, `nuestro_estudio_pt`, `titulo2_es`, `trabajo_bono_es`, `titulo2_en`, `trabajo_bono_en`, `titulo2_pt`, `trabajo_bono_pt`, `titulo3_es`, `postulaciones_es`, `titulo3_en`, `postulaciones_en`, `titulo3_pt`, `postulaciones_pt`, `text_slider_es`, `text_slider_en`, `text_slider_pt`) VALUES
(1, 'Nuestro Estudio', '<p>Lagos Maclean es una oficina de abogados para empresas. Nuestros abogados corporativos y litigantes cumplen estándares de eficacia y rapidez con apego ético y excelencia profesional. Áreas: retail, agroindustria, inmobiliario, construcción, educación, tecnología, telecomunicaciones, minería.</p>', 'Our Office', '<p>Lagos Maclean is a law office for Companies. Our Corporate and litigation attorneys fulfill the outmost standards of  efficiency and delivery on providing the services, all in compliance with Ethics and professional excellence required. Our main areas are: Retail, Agro Industry, Real Estate, Construction, Education, Technology, Telecommunications, mining.</p>', 'NOSSO ESCRITÓRIO', '<p>Lagos Maclean é um escritório jurídico especializado em direito de empresas. Nossos advogados cumprem com estandartes de eficiência e rapidez, sempre com  ética e excelência profissional. Áreas:  retail, agroindustrial, imobiliário, construção, educação, tecnologia, telecomunicações, mineração e outros.</p>', 'Responsabilidad Social Empresarial', '<p>Brindar asistencia legal gratuita a personas o instituciones que lo necesitan constituye un aspecto fundamental en el ejercicio de la profesión de abogado, nos permite cumplir con nuestro deber de servir a la justicia y garantizar el acceso imparcial a ella.</p>', 'Corporate Social Responsibility', '<p>Provide free legal assistance to persons or institutions in need is a fundamental aspect of the law practice for any attorney. Providing the pro bono work allowed us to fulfill our duty to serve justice and warrant the Impartial access to her.</p>', 'Responsabilidade Social Corporativa', '<p>Prestar assistência jurídica gratuita a pessoas ou instituições que precisam é um aspecto fundamental do exercício da profissão do advogado, e que permite cumprir o dever de servir à justiça e garantir seu acesso de forma equitativa.</p>', 'Socios', '<p>Si estás interesado en trabajar con nosotros envíanos tu CV:</p>', 'Partnership', '<p>If you are interested in working with us please send your CV:</p>', 'Parcería', '<p>Faça parte da nossa equipe. Mande seu CV a info@lmabogados.cl</p>', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE `novedades` (
  `id` int(11) NOT NULL,
  `titulo_es` varchar(255) NOT NULL,
  `titulo_en` varchar(255) NOT NULL,
  `titulo_pt` varchar(255) NOT NULL,
  `descripcion_es` longtext NOT NULL,
  `descripcion_en` longtext NOT NULL,
  `descripcion_pt` longtext NOT NULL,
  `imagen_home` varchar(255) NOT NULL,
  `imagen_interior` varchar(255) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`id`, `titulo_es`, `titulo_en`, `titulo_pt`, `descripcion_es`, `descripcion_en`, `descripcion_pt`, `imagen_home`, `imagen_interior`, `activo`, `created`) VALUES
(7, 'Incorporación de nuevo socio de Lagos Maclean', 'Incorporación de nuevo socio de Lagos Maclean', 'Incorporación de nuevo socio de Lagos Maclean', '<p style=\"text-align: right; \">Santiago, Mayo 2014.</p>\r\n \r\n<p>El abogado Raimundo Allende Zañartu, se ha incorporado como socio de Lagos Maclean.</p>\r\n \r\n<p>Allende aportará sus más de 20 años de experiencia profesional en los sectores de Retail y Telecomunicaciones, industrias en las que fue Abogado Jefe de las empresas SMU S.A y Walmart Chile y abogado de la Fiscalía de Telefónica S.A.</p>', '<p style=\"text-align: right; \">Santiago, Mayo 2014.</p>\r\n \r\n<p>El abogado Raimundo Allende Zañartu, se ha incorporado como socio de Lagos Maclean.</p>\r\n \r\n<p>Allende aportará sus más de 20 años de experiencia profesional en los sectores de Retail y Telecomunicaciones, industrias en las que fue Abogado Jefe de las empresas SMU S.A y Walmart Chile y abogado de la Fiscalía de Telefónica S.A.</p>', '<p style=\"text-align: right; \">Santiago, Mayo 2014.</p>\r\n \r\n<p>El abogado Raimundo Allende Zañartu, se ha incorporado como socio de Lagos Maclean.</p>\r\n \r\n<p>Allende aportará sus más de 20 años de experiencia profesional en los sectores de Retail y Telecomunicaciones, industrias en las que fue Abogado Jefe de las empresas SMU S.A y Walmart Chile y abogado de la Fiscalía de Telefónica S.A.</p>', '1426691679.jpg', '1432944383.jpg', 1, '2015-03-13 18:48:33'),
(9, 'Viaje de Abogado Socio a la International Chamber of Commerce', 'Viaje de Abogado Socio a la International Chamber of Commerce', 'Viaje de Abogado Socio a la International Chamber of Commerce', '<p style=\"text-align: right;\">Santiago, Junio 2014.</p>\r\n \r\n<p>El socio de Lagos Maclean Abogados, Kenneth Maclean, participará de un programa que desarrollará la Corte Internacional de Arbitraje de la ICC (International Chamber of Commerce), el día 30 de junio de 2014, en la ciudad de París.</p>\r\n \r\n<p>Los asistentes tendrán la oportunidad de escuchar de parte del secretario General, Consejeros y otros representantes del Servicio de Solución de Controversias, respecto del trabajo más reciente de la Comisión de Arbitraje y las diferentes áreas de ADR.</p>', '<p style=\"text-align: right; \">Santiago, Junio 2014.</p>\r\n \r\n<p>El socio de Lagos Maclean Abogados, Kenneth Maclean, participará de un programa que desarrollará la Corte Internacional de Arbitraje de la ICC (International Chamber of Commerce), el día 30 de junio de 2014, en la ciudad de París.</p>\r\n \r\n<p>Los asistentes tendrán la oportunidad de escuchar de parte del secretario General, Consejeros y otros representantes del Servicio de Solución de Controversias, respecto del trabajo más reciente de la Comisión de Arbitraje y las diferentes áreas de ADR.</p>', '<p style=\"text-align: right;\">Santiago, Junio 2014.</p>\r\n \r\n<p>El socio de Lagos Maclean Abogados, Kenneth Maclean, participará de un programa que desarrollará la Corte Internacional de Arbitraje de la ICC (International Chamber of Commerce), el día 30 de junio de 2014, en la ciudad de París.</p>\r\n \r\n<p>Los asistentes tendrán la oportunidad de escuchar de parte del secretario General, Consejeros y otros representantes del Servicio de Solución de Controversias, respecto del trabajo más reciente de la Comisión de Arbitraje y las diferentes áreas de ADR.</p>', '1426691707.jpg', '1426691973.jpg', 1, '2015-03-13 19:08:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nuestro_estudios`
--

CREATE TABLE `nuestro_estudios` (
  `id` int(11) NOT NULL,
  `mision_titulo_es` text NOT NULL,
  `mision_descrip_es` text NOT NULL,
  `mision_titulo_en` text NOT NULL,
  `mision_descrip_en` text NOT NULL,
  `mision_titulo_pt` text NOT NULL,
  `mision_descrip_pt` text NOT NULL,
  `vision_titulo_es` text NOT NULL,
  `vision_descrip_es` text NOT NULL,
  `vision_titulo_en` text NOT NULL,
  `vision_descrip_en` text NOT NULL,
  `vision_titulo_pt` text NOT NULL,
  `vision_descrip_pt` text NOT NULL,
  `n_valores_es` text NOT NULL,
  `n_valores_en` text NOT NULL,
  `n_valores_pt` text NOT NULL,
  `confianza_es` text NOT NULL,
  `confianza_des_es` text NOT NULL,
  `confianza_en` text NOT NULL,
  `confianza_des_en` text NOT NULL,
  `confianza_pt` text NOT NULL,
  `confianza_des_pt` text NOT NULL,
  `eficiencia_es` text NOT NULL,
  `eficiencia_des_es` text NOT NULL,
  `eficiencia_en` text NOT NULL,
  `eficiencia_des_en` text NOT NULL,
  `eficiencia_pt` text NOT NULL,
  `eficiencia_des_pt` text NOT NULL,
  `compromiso_es` text NOT NULL,
  `compromiso_des_es` text NOT NULL,
  `compromiso_en` text NOT NULL,
  `compromiso_des_en` text NOT NULL,
  `compromiso_pt` text NOT NULL,
  `compromiso_des_pt` text NOT NULL,
  `equipo_es` text NOT NULL,
  `equipo_des_es` text NOT NULL,
  `equipo_en` text NOT NULL,
  `equipo_des_en` text NOT NULL,
  `equipo_pt` text NOT NULL,
  `equipo_des_pt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nuestro_estudios`
--

INSERT INTO `nuestro_estudios` (`id`, `mision_titulo_es`, `mision_descrip_es`, `mision_titulo_en`, `mision_descrip_en`, `mision_titulo_pt`, `mision_descrip_pt`, `vision_titulo_es`, `vision_descrip_es`, `vision_titulo_en`, `vision_descrip_en`, `vision_titulo_pt`, `vision_descrip_pt`, `n_valores_es`, `n_valores_en`, `n_valores_pt`, `confianza_es`, `confianza_des_es`, `confianza_en`, `confianza_des_en`, `confianza_pt`, `confianza_des_pt`, `eficiencia_es`, `eficiencia_des_es`, `eficiencia_en`, `eficiencia_des_en`, `eficiencia_pt`, `eficiencia_des_pt`, `compromiso_es`, `compromiso_des_es`, `compromiso_en`, `compromiso_des_en`, `compromiso_pt`, `compromiso_des_pt`, `equipo_es`, `equipo_des_es`, `equipo_en`, `equipo_des_en`, `equipo_pt`, `equipo_des_pt`) VALUES
(1, 'MISIÓN', '<p>Lagos Maclean presta servicios legales a empresas, con excelencia, eficacia y rapidez, cautelando un elevado estándar ético y profesional, privilegiando las relaciones de largo plazo y de acuerdo a las necesidades particulares de cada cliente.</p>', 'MISSION', '<p>Lagos Maclean provides legal services to companies with Excellency, Efficiency and the best Delivery, all in a frame of the highest Ethical and Professional standard having in mind long term relationships  in accordance to our clients particular needs.</p>', 'MISSÃO', '<p>Lagos Maclean presta serviços jurídicos a empresas, com excelência, eficiência e rapidez, garantindo um alto padrão ético e profissional, buscando sempre relações de longo prazo e de acordo com as necessidades específicas de cada cliente.</p>', 'VISIÓN', '<p>Lagos Maclean cuenta con un prestigio ampliamente reconocido por la calidad de sus servicios y su capacidad para multiplicar valor para sus socios y grupos de interés.</p>', 'VISION', '<p>Lagos Maclean has a well known prestige and is fully recognized for its capacities the quality  of its services and the capability to multiply value for his partners and stakeholders.</p>', 'VISÃO', '<p>Lagos Maclean conta com prestigio amplamente reconhecido pela qualidade dos seus serviços prestados e pela capacidade de criar valor para seus sócios, parceiros e grupos de interesse.</p>', 'NUESTROS VALORES', 'OUR VALUES', 'Nossos Valores', 'Confianza', '        <ul>                                <li>Integridad</li>                                <li>Seriedad</li>                                <li>Transparencia</li>                             <li>Cumplimiento de la norma</li>                            </ul>', 'Trustable', '<ul>                                <li>Integrity</li>                                <li>Serious</li>                                <li>Transparence</li>                             <li>Compliance</li>                            </ul>', 'Confiança', '<ul>\r\n<li>Integridade</li>\r\n<li>Seriedade</li>\r\n<li>Transparência</li>\r\n<li>Cumprimento da norma.</li>\r\n</ul>', 'Eficiencia', '        <ul>                                <li>Laboriosidad</li>\r\n<li>Estudio</li>\r\n<li>Innovación </li>\r\n</ul>', 'Efficiency', '<ul>\r\n<li>Hard work</li>\r\n<li>Study</li>\r\n<li>Innovation</li>\r\n</ul>', 'Eficiência', '<ul>\r\n<li>Trabalho</li>\r\n<li>Estudo</li>\r\n<li>Inovação</li>\r\n</ul>', 'Compromiso', '        <ul>                                <li>Servicio al cliente</li>                                <li>Responsabilidad</li><li>Dedicación</li>\r\n<li>Vocación</li>\r\n<li>Preocupación por las personas</li>\r\n</ul>', 'Commitment', '<ul>\r\n<li>Client service</li>\r\n<li>Responsibility</li>\r\n<li>Dedication</li>\r\n<li>Vocation</li>\r\n<li>Care for persons</li>\r\n</ul>', 'Compromisso ', '<ul>\r\n<li>Atendimento ao Cliente</li>\r\n<li>Responsabilidade</li>\r\n<li>Dedicação</li>\r\n<li>Vocação</li>\r\n<li>Preocupação com as pessoas</li>\r\n</ul>', 'Equipo', '        <ul>                                <li>Autocrítica</li>                                <li>Lealtad</li>                                <li>Colaboración</li>                             </ul>\r\n\r\n\r\n\r\n', 'Team', '<ul>\r\n<li>Self-criticism</li>\r\n<li>Loyalty</li>\r\n<li>Collaboration</li>\r\n<li>Care for persons</li>\r\n</ul>', 'Equipe ', '<ul>\r\n<li>Autocrítica</li>\r\n<li>Lealdade</li>\r\n<li>Colaboração.</li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabras`
--

CREATE TABLE `palabras` (
  `id` int(11) NOT NULL,
  `leer_mas_es` varchar(255) NOT NULL,
  `leer_mas_en` varchar(255) NOT NULL,
  `leer_mas_pt` varchar(255) NOT NULL,
  `haz_click_es` varchar(255) NOT NULL,
  `haz_click_en` varchar(255) NOT NULL,
  `haz_click_pt` varchar(255) NOT NULL,
  `ver_es` varchar(255) NOT NULL,
  `ver_en` varchar(255) NOT NULL,
  `ver_pt` varchar(255) NOT NULL,
  `nombre_es` varchar(255) NOT NULL,
  `nombre_en` varchar(255) NOT NULL,
  `nombre_pt` varchar(255) NOT NULL,
  `email_es` varchar(255) NOT NULL,
  `email_en` varchar(255) NOT NULL,
  `email_pt` varchar(255) NOT NULL,
  `asunto_es` varchar(255) NOT NULL,
  `asunto_en` varchar(255) NOT NULL,
  `asunto_pt` varchar(255) NOT NULL,
  `mensaje_es` varchar(255) NOT NULL,
  `mensaje_en` varchar(255) NOT NULL,
  `mensaje_pt` varchar(255) NOT NULL,
  `enviar_es` varchar(255) NOT NULL,
  `enviar_en` varchar(255) NOT NULL,
  `enviar_pt` varchar(255) NOT NULL,
  `volver_es` varchar(255) NOT NULL,
  `volver_en` varchar(255) NOT NULL,
  `volver_pt` varchar(255) NOT NULL,
  `sin_filtro_es` varchar(255) NOT NULL,
  `sin_filtro_en` varchar(255) NOT NULL,
  `sin_filtro_pt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `palabras`
--

INSERT INTO `palabras` (`id`, `leer_mas_es`, `leer_mas_en`, `leer_mas_pt`, `haz_click_es`, `haz_click_en`, `haz_click_pt`, `ver_es`, `ver_en`, `ver_pt`, `nombre_es`, `nombre_en`, `nombre_pt`, `email_es`, `email_en`, `email_pt`, `asunto_es`, `asunto_en`, `asunto_pt`, `mensaje_es`, `mensaje_en`, `mensaje_pt`, `enviar_es`, `enviar_en`, `enviar_pt`, `volver_es`, `volver_en`, `volver_pt`, `sin_filtro_es`, `sin_filtro_en`, `sin_filtro_pt`) VALUES
(1, 'Leer más', 'more', 'leia mais', 'Haz click aquí', 'click here', 'clique aqui', 'Consultar', 'Consult', 'Consultar', 'Nombre:', 'name:', 'Nome:', 'Email:', 'email:', 'E-mail:', 'Asunto:', 'issue:', 'Assunto:', 'Mensaje:', 'message:', 'MENSAGEM:', 'Enviar', 'send', 'Enviar', 'volver', 'back', 'voltar', 'Todas las categorías', 'All categories', 'Todas as categorias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabras_menus`
--

CREATE TABLE `palabras_menus` (
  `id` int(11) NOT NULL,
  `inicio_es` text NOT NULL,
  `nuestro_estudio_es` text NOT NULL,
  `area_practica_es` text NOT NULL,
  `equipo_es` text NOT NULL,
  `red_es` varchar(255) NOT NULL,
  `novedades_es` text NOT NULL,
  `contacto_es` text NOT NULL,
  `inicio_en` text NOT NULL,
  `nuestro_estudio_en` text NOT NULL,
  `area_practica_en` text NOT NULL,
  `equipo_en` text NOT NULL,
  `red_en` varchar(255) NOT NULL,
  `novedades_en` text NOT NULL,
  `contacto_en` text NOT NULL,
  `inicio_pt` text NOT NULL,
  `nuestro_estudio_pt` text NOT NULL,
  `area_practica_pt` text NOT NULL,
  `equipo_pt` text NOT NULL,
  `red_pt` varchar(255) NOT NULL,
  `novedades_pt` text NOT NULL,
  `contacto_pt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `palabras_menus`
--

INSERT INTO `palabras_menus` (`id`, `inicio_es`, `nuestro_estudio_es`, `area_practica_es`, `equipo_es`, `red_es`, `novedades_es`, `contacto_es`, `inicio_en`, `nuestro_estudio_en`, `area_practica_en`, `equipo_en`, `red_en`, `novedades_en`, `contacto_en`, `inicio_pt`, `nuestro_estudio_pt`, `area_practica_pt`, `equipo_pt`, `red_pt`, `novedades_pt`, `contacto_pt`) VALUES
(1, 'inicio', 'nuestro estudio', 'áreas de práctica', 'equipo', 'red', 'novedades', 'contacto', 'home', 'our office', 'practice areas', 'team', 'net', 'news', 'contact', 'INÍCIO', 'NOSSO ESCRITÓRIO ', 'ÁREAS DE ATUAÇÃO ', 'EQUIPE ', 'rede', 'NOTÍCIA', 'CONTATO'),
(2, 'inicio5555', 'nuestro estudio', 'áreas de practica', 'equipo', '', 'novedades', 'contacto', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'inicio5555', 'nuestro estudio', 'áreas de practica', 'equipo', '', 'novedades', 'contacto', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes`
--

CREATE TABLE `redes` (
  `id` int(11) NOT NULL,
  `text_es` mediumtext NOT NULL,
  `text_en` mediumtext NOT NULL,
  `text_pt` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `redes`
--

INSERT INTO `redes` (`id`, `text_es`, `text_en`, `text_pt`) VALUES
(1, '<p>Somos un estudio de tamaño medio, que desarrolla su trabajo de manera asertiva y también flexible frente a las necesidades del cliente. Así, nuestros equipos de trabajo combinan la experiencia, la efectividad y la energía de nuestros abogados.<br /><br />Nuestras oficinas se encuentran ubicadas en el principal distrito empresarial de la ciudad de Santiago de Chile, Barrio El Golf, y cuenta con una adecuada infraestructura física, de capital humano y soporte administrativo para la prestación de servicios conforme los requerimientos de nuestros clientes.<br /><br />\r\nLagos Maclean Abogados presta sus servicios con cobertura nacional de forma directa dentro del territorio de la República de Chile, lo que se ejecuta con el soporte de una amplia red de abogados delegados. <br /><br />\r\nSin perjuicio de lo anterior, tenemos una oficina propia en la ciudad de Antofagasta, con lo cual prestaremos servicios directamente en dicha plaza y en la ciudad de Calama. A este efecto un socio de nuestro estudio está a cargo de dicha oficina, contando con el apoyo y servicio de nuestra oficina principal en la ciudad de Santiago de Chile. <br /><br /> \r\nDesde el punto de vista internacional, contamos con una red de oficinas a través de la cuales prestamos servicios en diversos países de la región. <br /><br />\r\nAsimismo, prestamos servicio con cobertura en todo el territorio de la República Federativa de Brasil, a través de una asociación con la oficina de abogados Nelson Wilians & Advogados Associados (www.nwadv.com.br), que cuenta con mas de 44 oficinas propias a lo largo del territorio brasileño.</p>', '<p>We are a medium sized studio, developing their work creatively, assertively and flexibly to customer needs. Thus, our teams combine expertise, effectiveness and maximum energy of our lawyers. <br /><br />Our offices are located in the main business district of the city of Santiago de Chile, Barrio El Golf, and has adequate physical infrastructure, human capital and management support for the provision of services under the requirements of our customers. <br /><br />\r\nLagos Maclean Attorneys serves national coverage directly within the territory of the Republic of Chile, which is run with the support of a wide network of delegate lawyer. <br /><br />\r\nNotwithstanding the above, we have an office in Antofagasta, which provide legal services directly in that city and the city of Calama. For this purpose a partner of our study is in charge of that office , with the support and service of our main office in the city of Santiago of Chile. <br /><br />\r\nFrom the international point of view , we have a network of offices through which we provide services in several countries in the region. <br /><br />\r\nWe also provide service coverage within the territory of the Federal Republic of Brazil, through a partnership with the law firm Nelson Wilians & Advogados Associados (www.nwadv.com.br), which has more than 44 offices throughout the Brazilian territory.</p>', '<p>Somos um escritório de médio porte, que desenvolve seu trabalho de forma criativa, certeira e flexível frente às necessidades do cliente.  Deste modo, nossas equipes de trabalho combinam  a experiência, a efetividade e a energia dos nossos advogados. <br /><br />Nosso escritório encontra-se no principal distrito empresarial da cidade de Santiago do Chile, no Bairro El Golf, e conta com uma excelente infraestrutura física, de capital humano e suporte administrativo par prestar os serviços requeridos por nossos clientes. <br /><br />\r\nLagos Maclean Abogados presta seus serviços com cobertura nacional de forma direta dentro do território da República do Chile, para isso, conta com o suporte de uma ampla rede de advogados delegados. <br /><br />\r\nIndependente do anterior, contamos com um escritório em Antofagasta, com o qual prestaremos serviços nesta cidade e em Calama. Um dos nossos sócios é  o responsável pelo escritório, contando com o apoio e serviço da matriz em Santiago do Chile. <br /><br />\r\nDo ponto de vista internacional, contamos com uma rede de escritórios através das quais prestamos serviços em diversos países da região.<br /> <br />\r\nAdicionalmente, prestamos serviço com cobertura em todo o território da República Federativa de Brasil, através de uma associação com Nelson Wilians & Advogados Associados (www.nwadv.com.br), que conta com mais de 44 escritórios próprios em todo o território brasileiro.</p>');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area_de_practicas`
--
ALTER TABLE `area_de_practicas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `equipos_des`
--
ALTER TABLE `equipos_des`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inicios`
--
ALTER TABLE `inicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nuestro_estudios`
--
ALTER TABLE `nuestro_estudios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `palabras`
--
ALTER TABLE `palabras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `palabras_menus`
--
ALTER TABLE `palabras_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `redes`
--
ALTER TABLE `redes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area_de_practicas`
--
ALTER TABLE `area_de_practicas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT de la tabla `equipos_des`
--
ALTER TABLE `equipos_des`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `footers`
--
ALTER TABLE `footers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `inicios`
--
ALTER TABLE `inicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `novedades`
--
ALTER TABLE `novedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `nuestro_estudios`
--
ALTER TABLE `nuestro_estudios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `palabras`
--
ALTER TABLE `palabras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `palabras_menus`
--
ALTER TABLE `palabras_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
