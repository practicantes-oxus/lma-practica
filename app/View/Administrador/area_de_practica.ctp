<?php $this->append("activePractica"); echo "active"; $this->end(); ?>
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i> Áreas de práctica</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_practica"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id" value="<?php echo $checkArea ["AreaDePractica"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["titulo_area_es"]; ?>" name="titulo_area_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="area_descrip_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote es"><?php echo $checkArea ["AreaDePractica"]["area_descrip_es"]; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t1_es"]; ?>" name="t1_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t1_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t1 es"><?php echo $checkArea ["AreaDePractica"]["t1_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t6_es"]; ?>" name="t6_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t6_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t6 es"><?php echo $checkArea ["AreaDePractica"]["t6_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t5_es"]; ?>" name="t5_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t5_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t5 es"><?php echo $checkArea ["AreaDePractica"]["t5_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t3_es"]; ?>" name="t3_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t3_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t3 es"><?php echo $checkArea ["AreaDePractica"]["t3_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">5:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t2_es"]; ?>" name="t2_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t2_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t2 es"><?php echo $checkArea ["AreaDePractica"]["t2_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">6:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t7_es"]; ?>" name="t7_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t7_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t7 es"><?php echo $checkArea ["AreaDePractica"]["t7_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">7:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t4_es"]; ?>" name="t4_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t4_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t4 es"><?php echo $checkArea ["AreaDePractica"]["t4_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">8:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t8_es"]; ?>" name="t8_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t8_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t8 es"><?php echo $checkArea ["AreaDePractica"]["t8_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["titulo_area_en"]; ?>" name="titulo_area_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="area_descrip_en" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote en"><?php echo $checkArea ["AreaDePractica"]["area_descrip_en"]; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t1_en"]; ?>" name="t1_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t1_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t1 en"><?php echo $checkArea ["AreaDePractica"]["t1_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t6_en"]; ?>" name="t6_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t6_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t6 en"><?php echo $checkArea ["AreaDePractica"]["t6_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t5_en"]; ?>" name="t5_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t5_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t5 en"><?php echo $checkArea ["AreaDePractica"]["t5_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t3_en"]; ?>" name="t3_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t3_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t3 en"><?php echo $checkArea ["AreaDePractica"]["t3_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">5:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t2_en"]; ?>" name="t2_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t2_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t2 en"><?php echo $checkArea ["AreaDePractica"]["t2_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">6:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t7_en"]; ?>" name="t7_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t7_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t7 en"><?php echo $checkArea ["AreaDePractica"]["t7_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">7:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t4_en"]; ?>" name="t4_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t4_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t4 en"><?php echo $checkArea ["AreaDePractica"]["t4_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">8:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t8_en"]; ?>" name="t8_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t8_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t8 en"><?php echo $checkArea ["AreaDePractica"]["t8_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["titulo_area_pt"]; ?>" name="titulo_area_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="area_descrip_pt" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote pt"><?php echo $checkArea ["AreaDePractica"]["area_descrip_pt"]; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t1_pt"]; ?>" name="t1_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t1_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t1 pt"><?php echo $checkArea ["AreaDePractica"]["t1_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t6_pt"]; ?>" name="t6_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t6_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t6 pt"><?php echo $checkArea ["AreaDePractica"]["t6_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t5_pt"]; ?>" name="t5_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t5_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t5 pt"><?php echo $checkArea ["AreaDePractica"]["t5_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t3_pt"]; ?>" name="t3_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t3_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t3 pt"><?php echo $checkArea ["AreaDePractica"]["t3_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">5:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t2_pt"]; ?>" name="t2_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t2_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t2 pt"><?php echo $checkArea ["AreaDePractica"]["t2_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">6:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t7_pt"]; ?>" name="t7_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t7_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t7 pt"><?php echo $checkArea ["AreaDePractica"]["t7_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">7:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t4_pt"]; ?>" name="t4_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t4_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t4 pt"><?php echo $checkArea ["AreaDePractica"]["t4_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">8:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkArea ["AreaDePractica"]["t8_pt"]; ?>" name="t8_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="t8_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote t8 pt"><?php echo $checkArea ["AreaDePractica"]["t8_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_practica">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 100
            });
            $(".btn-save_practica").click(function(){
                var button = $(this);
                button.parents(".block-web").find("form textarea[name='area_descrip_es']").text(button.parents(".block-web").find("form .summernote.es").code());
                button.parents(".block-web").find("form textarea[name='area_descrip_en']").text(button.parents(".block-web").find("form .summernote.en").code());
                button.parents(".block-web").find("form textarea[name='area_descrip_pt']").text(button.parents(".block-web").find("form .summernote.pt").code());

                button.parents(".block-web").find("form textarea[name='t1_des_es']").text(button.parents(".block-web").find("form .summernote.t1.es").code());
                button.parents(".block-web").find("form textarea[name='t2_des_es']").text(button.parents(".block-web").find("form .summernote.t2.es").code());
                button.parents(".block-web").find("form textarea[name='t3_des_es']").text(button.parents(".block-web").find("form .summernote.t3.es").code());
                button.parents(".block-web").find("form textarea[name='t4_des_es']").text(button.parents(".block-web").find("form .summernote.t4.es").code());
                button.parents(".block-web").find("form textarea[name='t5_des_es']").text(button.parents(".block-web").find("form .summernote.t5.es").code());
                button.parents(".block-web").find("form textarea[name='t6_des_es']").text(button.parents(".block-web").find("form .summernote.t6.es").code());
                button.parents(".block-web").find("form textarea[name='t7_des_es']").text(button.parents(".block-web").find("form .summernote.t7.es").code());
                button.parents(".block-web").find("form textarea[name='t8_des_es']").text(button.parents(".block-web").find("form .summernote.t8.es").code());

                button.parents(".block-web").find("form textarea[name='t1_des_en']").text(button.parents(".block-web").find("form .summernote.t1.en").code());
                button.parents(".block-web").find("form textarea[name='t2_des_en']").text(button.parents(".block-web").find("form .summernote.t2.en").code());
                button.parents(".block-web").find("form textarea[name='t3_des_en']").text(button.parents(".block-web").find("form .summernote.t3.en").code());
                button.parents(".block-web").find("form textarea[name='t4_des_en']").text(button.parents(".block-web").find("form .summernote.t4.en").code());
                button.parents(".block-web").find("form textarea[name='t5_des_en']").text(button.parents(".block-web").find("form .summernote.t5.en").code());
                button.parents(".block-web").find("form textarea[name='t6_des_en']").text(button.parents(".block-web").find("form .summernote.t6.en").code());
                button.parents(".block-web").find("form textarea[name='t7_des_en']").text(button.parents(".block-web").find("form .summernote.t7.en").code());
                button.parents(".block-web").find("form textarea[name='t8_des_en']").text(button.parents(".block-web").find("form .summernote.t8.en").code());

                button.parents(".block-web").find("form textarea[name='t1_des_pt']").text(button.parents(".block-web").find("form .summernote.t1.pt").code());
                button.parents(".block-web").find("form textarea[name='t2_des_pt']").text(button.parents(".block-web").find("form .summernote.t2.pt").code());
                button.parents(".block-web").find("form textarea[name='t3_des_pt']").text(button.parents(".block-web").find("form .summernote.t3.pt").code());
                button.parents(".block-web").find("form textarea[name='t4_des_pt']").text(button.parents(".block-web").find("form .summernote.t4.pt").code());
                button.parents(".block-web").find("form textarea[name='t5_des_pt']").text(button.parents(".block-web").find("form .summernote.t5.pt").code());
                button.parents(".block-web").find("form textarea[name='t6_des_pt']").text(button.parents(".block-web").find("form .summernote.t6.pt").code());
                button.parents(".block-web").find("form textarea[name='t7_des_pt']").text(button.parents(".block-web").find("form .summernote.t7.pt").code());
                button.parents(".block-web").find("form textarea[name='t8_des_pt']").text(button.parents(".block-web").find("form .summernote.t8.pt").code());

                button.parents(".block-web").find("form input[type='submit']").click();
            });
        });
    </script>
<?php $this->end(); ?>