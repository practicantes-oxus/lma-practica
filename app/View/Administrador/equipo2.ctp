<?php $this->append("activeEquipo2"); echo "active"; $this->end(); ?>
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i> Equipo</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_equipo_desc"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id" value="<?php echo $checkEquipo ["EquiposDes"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <textarea class="hidden" name="descrip_es"></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote es"><?php echo $checkEquipo ["EquiposDes"]["descrip_es"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <textarea class="hidden" name="descrip_en"></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote en"><?php echo $checkEquipo ["EquiposDes"]["descrip_en"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <textarea class="hidden" name="descrip_pt"></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote pt"><?php echo $checkEquipo ["EquiposDes"]["descrip_pt"]; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_equipo">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 200
            });
            $(".btn-save_equipo").click(function(){
                var button = $(this);
                button.parents(".block-web").find("form textarea[name='descrip_es']").text(button.parents(".block-web").find("form .summernote.es").code());
                button.parents(".block-web").find("form textarea[name='descrip_en']").text(button.parents(".block-web").find("form .summernote.en").code());
                button.parents(".block-web").find("form textarea[name='descrip_pt']").text(button.parents(".block-web").find("form .summernote.pt").code());
                button.parents(".block-web").find("form input[type='submit']").click();
            });
        });
    </script>
<?php $this->end(); ?>