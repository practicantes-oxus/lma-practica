<?php $this->append("activeFooter"); echo "active"; $this->end(); ?>
<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-tasks"></i> Pie de pagina</h2>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="porlets-content">
                    <div class="clearfix">
                        <div class="btn-group">
                            <button class="btn btn-primary" data-backdrop="static" data-toggle="modal" data-target="#add_case">Añadir imagen <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="margin-top-10"></div>
                    <div class="table-responsive">
                        <table class="display table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="10%" class="text-center">#</th>
                                <th width="50%" class="text-center">Imagen</th>
                                <th width="40%" class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($checkFooter as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $this->Html->image("footer/" . $value["Footer"]["imagen"]); ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php echo $this->Html->link("<i class='fa fa-pencil'></i>", "#", array("class" => "btn btn-xs btn-default btn-edit", "data-id" => $value["Footer"]["id"], "data-toggle" => "tooltip", "title" => "Editar", "escape" => false)) ?>
                                            <?php echo $this->Html->link("<i class='fa fa-times'></i>", "#", array("class" => "btn btn-xs btn-default btn-delete", "data-id" => $value["Footer"]["id"], "data-type" => "Footer", "data-target" => "delete_product", "data-toggle" => "tooltip", "title" => "Eliminar", "escape" => false)) ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->append("modal"); ?>
    <div class="modal fade" id="add_case" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Nueva cv</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo $this->webroot . "extraAdministrador/save_footer"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <input class="hidden" name="id">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Imagen:</label>
                            <div class="col-sm-10">
                                <input type="file" name="imagen" class="form-control" id="inputPassword3">
                                <small><b>Medidas.- 159 x 49px</b></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Enlace:</label>
                            <div class="col-sm-10">

                                <input type="text" name="href" class="form-control">

                            </div>
                        </div>
                        <input type="submit" class="hidden" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary btn-save_case">Guardar</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->end(); ?>
<?php $this->append("extra-script"); ?>
<script>
    $(function() {
        $(".btn-save_case").click(function(){
            var button = $(this);
            button.parents(".modal").find("form input[type='submit']").click();
        });
        $(".btn-delete").click(function() {
            var button = $(this);
            var target = $("#delete");
            target.find("form input[name='id']").val(button.attr("data-id"));
            target.find("form input[name='type']").val(button.attr("data-type"));
            target.modal("show");
        });
        $(".btn-edit2").click(function() {
            var button, target;
            button = $(this);
            target = $("#add_case");
            target.find("form input").val("");
            $.post("<?php echo $this -> webroot . "extraAdministrador/get_footer";?>", {
                id: button.attr("data-id")
            }, function(response) {
                var json;
                json = $.parseJSON(response);
                target.find("form input[name='id']").val(json.Categoria.id);
                target.find("form input[name='href']").val(json.Categoria.href);
            }).done(function() {
                return target.modal({
                    backdrop: "static"
                }, "show");
            });
        });
    });
</script>
<?php $this->end(); ?>