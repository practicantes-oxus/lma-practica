<?php $this->append("activeIndex"); echo "active"; $this->end(); ?>
<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-tasks"></i> Novedades</h2>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="porlets-content">
                    <div class="clearfix">
                        <div class="btn-group">
                            <button class="btn btn-primary" data-backdrop="static" data-toggle="modal" data-target="#add_case">Añadir novedad <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="margin-top-10"></div>
                    <div class="table-responsive">
                        <table class="display table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="10%" class="text-center">#</th>
                                <th width="50%" class="text-center">Titulo</th>
                                <th width="40%" class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach( $checkRow as $key => $value ) { ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td>
                                        <span class="label label-primary">Español: <?php echo $value["Novedade"]["titulo_es"]; ?></span>
                                        <span class="label label-success">Ingles: <?php echo $value["Novedade"]["titulo_en"]; ?></span>
                                        <span class="label label-info">Portugués: <?php echo $value["Novedade"]["titulo_pt"]; ?></span>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <?php echo $this->Html->link("<i class='fa fa-pencil'></i>", "#", array("class" => "btn btn-xs btn-default btn-edit", "data-id" => $value["Novedade"]["id"], "data-target" => "edit_product", "data-toggle" => "tooltip", "title" => "Editar", "escape" => false)) ?>
                                            <?php echo $this->Html->link("<i class='fa fa-times'></i>", "#", array("class" => "btn btn-xs btn-default btn-delete", "data-id" => $value["Novedade"]["id"], "data-type" => "Novedade", "data-target" => "delete_product", "data-toggle" => "tooltip", "title" => "Eliminar", "escape" => false)) ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->append("modal"); ?>
    <div class="modal fade" id="add_case" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Nueva novedad</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo $this -> webroot . "extraAdministrador/save_novedades"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <input class="hidden" name="id">
                        <div id="progressWizard" class="cases-wizard">
                            <ul class="nav nav-pills nav-justified">
                                <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                                <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                                <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="progress progress-striped">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                                </div>
                                <div class="tab-pane active" id="ptab1">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Imagen home:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="imagen_home" class="form-control" id="inputPassword3" >
                                            <small><b>Medidas.- 340 x 124px</b></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Imagen Interior:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="imagen_interior" class="form-control" id="inputPassword3" >
                                            <small><b>Medidas.- 345 x 218px</b></small>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="titulo_es" class="form-control" id="inputPassword3" >
                                        </div>
                                    </div>
                                    <textarea class="hidden" name="descripcion_es" ></textarea>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                        <div class="col-sm-10">
                                            <div class="summernote es"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="ptab2">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="titulo_en" class="form-control" id="inputPassword3" >
                                        </div>
                                    </div>
                                    <textarea class="hidden" name="descripcion_en"></textarea>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                        <div class="col-sm-10">
                                            <div class="summernote en"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="ptab3">
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="titulo_pt" class="form-control" id="inputPassword3" >
                                        </div>
                                    </div>
                                    <textarea class="hidden" name="descripcion_pt"></textarea>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                        <div class="col-sm-10">
                                            <div class="summernote pt"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="hidden">
                    </form>
                </div>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary btn-save_case">Guardar</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->end(); ?>

<?php $this->append("extra-script"); ?>
<script>
    $(function() {
        $(".summernote").summernote({
            height: 200
        });
        $(".btn-save_case").click(function(){
           var button = $(this);
            button.parents(".modal").find("form textarea[name='descripcion_es']").text(button.parents(".modal").find("form .summernote.es").code());
            button.parents(".modal").find("form textarea[name='descripcion_en']").text(button.parents(".modal").find("form .summernote.en").code());
            button.parents(".modal").find("form textarea[name='descripcion_pt']").text(button.parents(".modal").find("form .summernote.pt").code());
            button.parents(".modal").find("form input[type='submit']").click();
        });
        $(".btn-delete").click(function() {
            var button = $(this);
            var target = $("#delete");
            target.find("form input[name='id']").val(button.attr("data-id"));
            target.find("form input[name='type']").val(button.attr("data-type"));
            target.modal("show");
        });
        $(".btn-edit").click(function() {
            var button, target;
            button = $(this);
            target = $("#add_case");
            target.find("form input").val("");
            $.post("<?php echo $this -> webroot . "extraAdministrador/get_novedades";?>", {
                id: button.attr("data-id")
            }, function(response) {
                var json;
                json = $.parseJSON(response);
                target.find("form input[name='id']").val(json.Novedade.id);
                target.find("form input[name='titulo_es']").val(json.Novedade.titulo_es);
                target.find("form input[name='titulo_en']").val(json.Novedade.titulo_en);
                target.find("form input[name='titulo_pt']").val(json.Novedade.titulo_pt);
                target.find("form .summernote.es").code(json.Novedade.descripcion_es);
                target.find("form .summernote.en").code(json.Novedade.descripcion_en);
                target.find("form .summernote.pt").code(json.Novedade.descripcion_pt);
            }).done(function() {
                return target.modal({
                    backdrop: "static"
                }, "show");
            });
        });
    });
</script>
<?php $this->end(); ?>