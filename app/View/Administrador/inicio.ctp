<?php $this->append("activeInicio"); echo "active"; $this->end(); ?>
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i>Inicio</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_inicio"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id"  value="<?php echo $checkInicio ["Inicio"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo Slider:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["text_slider_es"]; ?>" name="text_slider_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo1_es"]; ?>" name="titulo1_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="nuestro_estudio_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote uno es"><?php echo $checkInicio ["Inicio"]["nuestro_estudio_es"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo2_es"]; ?>" name="titulo2_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="trabajo_bono_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote dos es"><?php echo $checkInicio ["Inicio"]["trabajo_bono_es"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo3_es"]; ?>" name="titulo3_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="postulaciones_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote tres es"><?php echo $checkInicio ["Inicio"]["postulaciones_es"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo Slider:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["text_slider_en"]; ?>" name="text_slider_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo1_en"]; ?>" name="titulo1_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="nuestro_estudio_en"></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote uno en"><?php echo $checkInicio ["Inicio"]["nuestro_estudio_en"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo2_en"]; ?>" name="titulo2_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="trabajo_bono_en" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote dos en"><?php echo $checkInicio ["Inicio"]["trabajo_bono_en"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo3_en"]; ?>" name="titulo3_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="postulaciones_en" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote tres en"><?php echo $checkInicio ["Inicio"]["postulaciones_en"]; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo Slider:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["text_slider_pt"]; ?>" name="text_slider_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo1_pt"]; ?>" name="titulo1_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="nuestro_estudio_pt"></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote uno pt"><?php echo $checkInicio ["Inicio"]["nuestro_estudio_pt"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo2_pt"]; ?>" name="titulo2_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="trabajo_bono_pt" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote dos pt"><?php echo $checkInicio ["Inicio"]["trabajo_bono_pt"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkInicio ["Inicio"]["titulo3_pt"]; ?>" name="titulo3_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="postulaciones_pt" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote tres pt"><?php echo $checkInicio ["Inicio"]["postulaciones_pt"]; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_case">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 200
            });
            $(".btn-save_case").click(function(){
                var button = $(this);
                button.parents(".block-web").find("form textarea[name='nuestro_estudio_es']").text(button.parents(".block-web").find("form .summernote.uno.es").code());
                button.parents(".block-web").find("form textarea[name='trabajo_bono_es']").text(button.parents(".block-web").find("form .summernote.dos.es").code());
                button.parents(".block-web").find("form textarea[name='postulaciones_es']").text(button.parents(".block-web").find("form .summernote.tres.es").code());

                button.parents(".block-web").find("form textarea[name='nuestro_estudio_en']").text(button.parents(".block-web").find("form .summernote.uno.en").code());
                button.parents(".block-web").find("form textarea[name='trabajo_bono_en']").text(button.parents(".block-web").find("form .summernote.dos.en").code());
                button.parents(".block-web").find("form textarea[name='postulaciones_en']").text(button.parents(".block-web").find("form .summernote.tres.en").code());

                button.parents(".block-web").find("form textarea[name='nuestro_estudio_pt']").text(button.parents(".block-web").find("form .summernote.uno.pt").code());
                button.parents(".block-web").find("form textarea[name='trabajo_bono_pt']").text(button.parents(".block-web").find("form .summernote.dos.pt").code());
                button.parents(".block-web").find("form textarea[name='postulaciones_pt']").text(button.parents(".block-web").find("form .summernote.tres.pt").code());

                button.parents(".block-web").find("form input[type='submit']").click();
            });
        });
    </script>
<?php $this->end(); ?>