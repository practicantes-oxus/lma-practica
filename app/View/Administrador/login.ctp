<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $this->Html->charset(); ?>

    <title>ADMINISTRADOR -LMA</title>
    <?php
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    echo $this->Html->css(array('bootstrap.min', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300', 'admin/font-awesome.min', 'admin/style', 'admin/main'));
    echo $this->fetch('css');
    ?>
</head>
<body style="">
    <div class="login-container">
        <div class="middle-login">
            <div class="block-web">
                <div class="head">
                    <?php echo $this->Html->image("logo.jpg", array("class" => "img-responsive", "width" => "150", "style" => "margin: 0 auto;")); ?>
                </div>
                <div style="background:#fff;">
                    <form action="<?php echo $this->webroot . "extraAdministrador/login"; ?>" method="post" class="form-horizontal" style="margin-bottom: 0px !important;">
                        <div class="content">
                            <?php echo $this->Session->flash("errorText"); ?>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group"><span class="input-group-addon"><i
                                                class="fa fa-user"></i></span>
                                        <input type="text" class="form-control" required name="username" id="username" placeholder="Username">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group"><span class="input-group-addon"><i
                                                class="fa fa-lock"></i></span>
                                        <input type="password" class="form-control" required name="password" id="password" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="foot">
                            <button type="submit" data-dismiss="modal" class="btn btn-primary">Ingresar</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-center out-links">Desarrollado por <?php echo $this->Html->link("", "http://boycot.cl", array("class" => "boycot", "target" => "_blank")); ?></div>
        </div>
    </div>
    <?php
        echo $this->Html->script(array('jquery-1.11.0.min', 'bootstrap.min', 'admin/accordion', 'admin/common-script', 'admin/jquery.nicescroll', 'admin/main'));
        echo $this->fetch('extra-script');
    ?>
</body>
</html>