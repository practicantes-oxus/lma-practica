<?php $this->append("activeNuestro"); echo "active"; $this->end(); ?>
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i> Nuestro Estudio</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_estudios"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id" value="<?php echo $checkEstudio ["NuestroEstudio"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["mision_titulo_es"]; ?>" name="mision_titulo_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="mision_descrip_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote m1 es"><?php echo $checkEstudio ["NuestroEstudio"]["mision_descrip_es"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["vision_titulo_es"]; ?>" name="vision_titulo_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="vision_descrip_es" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote v1 es"><?php echo $checkEstudio ["NuestroEstudio"]["vision_descrip_es"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["n_valores_es"]; ?>" name="n_valores_es" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["confianza_es"]; ?>" name="confianza_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="confianza_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote uno es"><?php echo $checkEstudio ["NuestroEstudio"]["confianza_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_es"]; ?>" name="eficiencia_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="eficiencia_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote dos es"><?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["compromiso_es"]; ?>" name="compromiso_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="compromiso_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote tres es"><?php echo $checkEstudio ["NuestroEstudio"]["compromiso_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["equipo_es"]; ?>" name="equipo_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="equipo_des_es" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote cuatro es"><?php echo $checkEstudio ["NuestroEstudio"]["equipo_des_es"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["mision_titulo_en"]; ?>" name="mision_titulo_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="mision_descrip_en" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote m2 en"><?php echo $checkEstudio ["NuestroEstudio"]["mision_descrip_en"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["vision_titulo_en"]; ?>" name="vision_titulo_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="vision_descrip_en" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote v2 en"><?php echo $checkEstudio ["NuestroEstudio"]["vision_descrip_en"]; ?></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["n_valores_en"]; ?>" name="n_valores_en" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["confianza_en"]; ?>" name="confianza_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="confianza_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote uno en"><?php echo $checkEstudio ["NuestroEstudio"]["confianza_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_en"]; ?>" name="eficiencia_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="eficiencia_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote dos en"><?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["compromiso_en"]; ?>" name="compromiso_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="compromiso_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote tres en"><?php echo $checkEstudio ["NuestroEstudio"]["compromiso_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["equipo_en"]; ?>" name="equipo_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="equipo_des_en" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote cuatro en"><?php echo $checkEstudio ["NuestroEstudio"]["equipo_des_en"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["mision_titulo_pt"]; ?>" name="mision_titulo_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="mision_descrip_pt" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote m3 pt"><?php echo $checkEstudio ["NuestroEstudio"]["mision_descrip_pt"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["vision_titulo_pt"]; ?>" name="vision_titulo_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <textarea class="hidden" name="vision_descrip_pt" ></textarea>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Descripción:</label>
                                    <div class="col-sm-10">
                                        <div class="summernote v3 pt"><?php echo $checkEstudio ["NuestroEstudio"]["vision_descrip_pt"]; ?></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo:</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["n_valores_pt"]; ?>" name="n_valores_pt" class="form-control" id="inputPassword3" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">1:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["confianza_pt"]; ?>" name="confianza_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="confianza_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote uno pt"><?php echo $checkEstudio ["NuestroEstudio"]["confianza_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">2:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_pt"]; ?>" name="eficiencia_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="eficiencia_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote dos pt"><?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">3:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["compromiso_pt"]; ?>" name="compromiso_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="compromiso_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote tres pt"><?php echo $checkEstudio ["NuestroEstudio"]["compromiso_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">4:</label>
                                            <div class="col-sm-8">
                                                <input type="text" value="<?php echo $checkEstudio ["NuestroEstudio"]["equipo_pt"]; ?>" name="equipo_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                        <textarea class="hidden" name="equipo_des_pt" ></textarea>
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-4 control-label">Descripción:</label>
                                            <div class="col-sm-8">
                                                <div class="summernote cuatro pt"><?php echo $checkEstudio ["NuestroEstudio"]["equipo_des_pt"]; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_case">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 100
            });
            $(".btn-save_case").click(function(){
                var button = $(this);
                button.parents(".block-web").find("form textarea[name='mision_descrip_es']").text(button.parents(".block-web").find("form .summernote.m1.es").code());
                button.parents(".block-web").find("form textarea[name='mision_descrip_en']").text(button.parents(".block-web").find("form .summernote.m2.en").code());
                button.parents(".block-web").find("form textarea[name='mision_descrip_pt']").text(button.parents(".block-web").find("form .summernote.m3.pt").code());

                button.parents(".block-web").find("form textarea[name='vision_descrip_es']").text(button.parents(".block-web").find("form .summernote.v1.es").code());
                button.parents(".block-web").find("form textarea[name='vision_descrip_en']").text(button.parents(".block-web").find("form .summernote.v2.en").code());
                button.parents(".block-web").find("form textarea[name='vision_descrip_pt']").text(button.parents(".block-web").find("form .summernote.v3.pt").code());

                button.parents(".block-web").find("form textarea[name='confianza_des_es']").text(button.parents(".block-web").find("form .summernote.uno.es").code());
                button.parents(".block-web").find("form textarea[name='confianza_des_en']").text(button.parents(".block-web").find("form .summernote.uno.en").code());
                button.parents(".block-web").find("form textarea[name='confianza_des_pt']").text(button.parents(".block-web").find("form .summernote.uno.pt").code());

                button.parents(".block-web").find("form textarea[name='eficiencia_des_es']").text(button.parents(".block-web").find("form .summernote.dos.es").code());
                button.parents(".block-web").find("form textarea[name='eficiencia_des_en']").text(button.parents(".block-web").find("form .summernote.dos.en").code());
                button.parents(".block-web").find("form textarea[name='eficiencia_des_pt']").text(button.parents(".block-web").find("form .summernote.dos.pt").code());

                button.parents(".block-web").find("form textarea[name='compromiso_des_es']").text(button.parents(".block-web").find("form .summernote.tres.es").code());
                button.parents(".block-web").find("form textarea[name='compromiso_des_en']").text(button.parents(".block-web").find("form .summernote.tres.en").code());
                button.parents(".block-web").find("form textarea[name='compromiso_des_pt']").text(button.parents(".block-web").find("form .summernote.tres.pt").code());

                button.parents(".block-web").find("form textarea[name='equipo_des_es']").text(button.parents(".block-web").find("form .summernote.cuatro.es").code());
                button.parents(".block-web").find("form textarea[name='equipo_des_en']").text(button.parents(".block-web").find("form .summernote.cuatro.en").code());
                button.parents(".block-web").find("form textarea[name='equipo_des_pt']").text(button.parents(".block-web").find("form .summernote.cuatro.pt").code());

                button.parents(".block-web").find("form input[type='submit']").click();
            });
        });
    </script>
<?php $this->end(); ?>