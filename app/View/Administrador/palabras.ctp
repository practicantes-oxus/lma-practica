<?php $this->append("activePalabras"); echo "active"; $this->end(); ?>
    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i> Áreas de práctica</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_palabras"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id" value="<?php echo $checkPalabras2 ["Palabra"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["leer_mas_es"]; ?>" name="leer_mas_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["haz_click_es"]; ?>" name="haz_click_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["ver_es"]; ?>" name="ver_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["sin_filtro_es"]; ?>" name="sin_filtro_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Contacto</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["nombre_es"]; ?>" name="nombre_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["email_es"]; ?>" name="email_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["asunto_es"]; ?>" name="asunto_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">7:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["mensaje_es"]; ?>" name="mensaje_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">8:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["enviar_es"]; ?>" name="enviar_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["leer_mas_en"]; ?>" name="leer_mas_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["haz_click_en"]; ?>" name="haz_click_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["ver_en"]; ?>" name="ver_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["sin_filtro_en"]; ?>" name="sin_filtro_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Contacto</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["nombre_en"]; ?>" name="nombre_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["email_en"]; ?>" name="email_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["asunto_en"]; ?>" name="asunto_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">7:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["mensaje_en"]; ?>" name="mensaje_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">8:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["enviar_en"]; ?>" name="enviar_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["leer_mas_pt"]; ?>" name="leer_mas_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["haz_click_pt"]; ?>" name="haz_click_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["ver_pt"]; ?>" name="ver_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["sin_filtro_pt"]; ?>" name="sin_filtro_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Contacto</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["nombre_pt"]; ?>" name="nombre_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["email_pt"]; ?>" name="email_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["asunto_pt"]; ?>" name="asunto_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">7:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["mensaje_pt"]; ?>" name="mensaje_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">8:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras2 ["Palabra"]["enviar_pt"]; ?>" name="enviar_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_palabras">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 100
            });
            $(".btn-save_palabras").click(function() {
                var button = $(this);
                button.parents(".block-web").find("form input[type='submit']").click();
            });
        });
    </script>
<?php $this->end(); ?>