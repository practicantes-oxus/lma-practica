<?php $this->append("activePalabras_menu"); echo "active"; $this->end(); ?>

    <div class="row">
        <div class="col-md-12">
            <h2><i class="fa fa-tasks"></i> Áreas de práctica</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <form action="<?php echo $this -> webroot . "extraAdministrador/save_palabras_menu"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <input class="hidden" name="id" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["id"]; ?>">
                    <div id="progressWizard" class="cases-wizard">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active"><a href="#ptab1" data-toggle="tab"><span>Paso 1:</span> Español</a></li>
                            <li><a href="#ptab2" data-toggle="tab"><span>Paso 2:</span> Ingles</a></li>
                            <li><a href="#ptab3" data-toggle="tab"><span>Paso 3:</span> Portugués</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="progress progress-striped">
                                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 66.66666666666666%;"></div>
                            </div>
                            <div class="tab-pane active" id="ptab1">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["inicio_es"]; ?>" name="inicio_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["nuestro_estudio_es"]; ?>" name="nuestro_estudio_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["area_practica_es"]; ?>" name="area_practica_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["equipo_es"]; ?>" name="equipo_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["novedades_es"]; ?>" name="novedades_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["contacto_es"]; ?>" name="contacto_es" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab2">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["inicio_en"]; ?>" name="inicio_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["nuestro_estudio_en"]; ?>" name="nuestro_estudio_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["area_practica_en"]; ?>" name="area_practica_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["equipo_en"]; ?>" name="equipo_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["novedades_en"]; ?>" name="novedades_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["contacto_en"]; ?>" name="contacto_en" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="ptab3">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">1:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["inicio_pt"]; ?>" name="inicio_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">2:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["nuestro_estudio_pt"]; ?>" name="nuestro_estudio_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">3:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["area_practica_pt"]; ?>" name="area_practica_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">4:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["equipo_pt"]; ?>" name="equipo_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">5:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["novedades_pt"]; ?>" name="novedades_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label">6:</label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?php echo $checkPalabras1 ["PalabrasMenu"]["contacto_pt"]; ?>" name="contacto_pt" class="form-control" id="inputPassword3" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="hidden">
                </form>
                <div class="modal-footer" style="display: none;">
                    <button type="button" class="btn btn-primary btn-save_menu">Guardar</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->append("extra-script"); ?>
    <script>
        $(function() {
            $(".summernote").summernote({
                height: 100
            });
            $(".btn-save_menu").click(function() {
                var button = $(this);
                button.parents(".block-web").find("form input[type='submit']").click();
            });

        });
    </script>
<?php $this->end(); ?>