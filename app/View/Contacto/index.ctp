<?php $this -> append("contacto"); echo "active"; $this -> end(); ?>
<style type="text/css">
    a{
        text-decoration: none !important;
        color: #000;
    }
    a:hover{
        color:#fd7600;
    }
    .panel{
        background-color: #fff;
        color: #fd7600;
    }
    .panel:hover{
        background-color: #fd7600;
        color: #fff;
    }
</style>
<div id="contacto">
    <br>
    <div class="row"  style="  margin-right: -30px;">
        <!--div class="row">
           <div class="col-xs-3 text-center panel panel-default" style="margin-top: 70px;">
               <div class="info panel-body" style="padding-top: 25px;">
                   <p>Isidora Goyenecha 3120, Piso 16</p>
                   <p>Las Condes, Santiago de Chile</p>
               </div>
           </div>
           <div class="col-xs-3 text-center panel panel-default" style="margin-top: 70px;">
               <div class="info panel-body" style="padding-top: 25px;">
                   <p>Orella 610, Piso 17, Of. 1703</p>
                   <p>Antofagasta</p>
               </div>
           </div>
           <div class="col-xs-3 text-center panel panel-default" style="margin-top: 70px;">
               <div class="info panel-body" style="padding-top: 25px;">
                   <p>Antonio Varas 216, Piso 10, OF. 1002</p>
                   <p>Puerto Montt</p>
               </div>
           </div>
           <div class="col-xs-3 text-center panel panel-default" style="margin-top: 70px;">
               <div class="info panel-body" style="padding-top: 25px;">
                   <p>Manuel Antonio Matta 520, oficina 502</p>
                   <p>Osorno.</p>
               </div>
           </div>
        </div-->
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <div class="box-1">
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="box-2">
                        <h2><?php echo $checkContacto ["Contacto"]["direccion_" . $language]; ?></h2>
                        <h4 style="color:black">Casa Matríz</h4>
                        <a target="_blank" href="https://goo.gl/maps/ABkmGyr8Fvv" title="Ver mapa"><?php echo $checkContacto ["Contacto"]["descrip_" . $language]; ?></a>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="row">
                        <!--a class="col-xs-6 text-center panel panel-default" href="https://goo.gl/maps/ABkmGyr8Fvv">
                            <div class="info panel-body" >
                               <p>Isidora Goyenecha 3120, Piso 16</p>
                               <p>Las Condes, Santiago de Chile</p>
                           </div>
                        </a-->
                        <a target="_blank" title="Ver mapa" class="col-xs-12 text-center panel panel-default" href="https://goo.gl/maps/UVkE6yWaeqA2">
                            <div class="info panel-body" >
                               <p>Orella 610, Piso 17, Of. 1703</p>
                               <p>Antofagasta</p>
                           </div>
                        </a>
                    <!--/div>
                    <div class="row"-->
                        <a target="_blank" title="Ver mapa" class="col-xs-12 text-center panel panel-default" href="https://goo.gl/maps/pRrZ66GU7DQ2">
                            <div class="info panel-body" >
                               <p>Antonio Varas 216, Piso 10, Of. 1002</p>
                               <p>Puerto Montt</p>
                           </div>
                        </a>
                        <a target="_blank" title="Ver mapa" class="col-xs-12 text-center panel panel-default" href="https://goo.gl/maps/TCPHxgC4aYE2">
                            <div class="info panel-body" >
                               <p>Manuel Antonio Matta 520, Of. 502</p>
                               <p>Osorno.</p>
                           </div>
                        </a>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('a').click(function(){

        });
    });
</script>