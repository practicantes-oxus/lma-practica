<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="<?php echo $this->webroot . "extraAdministrador/eliminar?continue=" . $this->here; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="id" />
        <input type="hidden" name="type" />
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar producto</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="checkboxAccount" required />
                                <span class="custom-checkbox"></span> ¿Estás seguro que lo deseas eliminar? </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                </div>
            </div>
        </div>
    </form>
</div>