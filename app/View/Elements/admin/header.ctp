<div class="header navbar navbar-inverse box-shadow navbar-fixed-top">
    <div class="navbar-inner">
        <div class="header-seperation">
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle-box"> <a href="#"><i class="fa fa-bars"></i></a> </li>
                <li><?php echo $this->Html->link("", array("controller" => "home", "action" => "index"), array("class" => "powerbrands")); ?></li>
                <li><?php echo $this->Html->link("Cerrar sesión <i class='fa fa-angle-double-right'></i>", array("controller" => "extraAdministrador", "action" => "logout"), array("escape" => false)); ?></li>
            </ul><!--/nav navbar-nav-->
        </div><!--/header-seperation-->
    </div><!--/navbar-inner-->
</div>