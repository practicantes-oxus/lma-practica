<div class="nav-collapse top-margin fixed box-shadow2 hidden-xs" id="sidebar">
        <ul id="nav-accordion" class="sidebar-menu">
            <li>
                <h3>General</h3>
            </li>
            <li><?php echo $this->Html->link("<i class='fa fa-bookmark'></i> Novedades", array("controller" => "administrador", "action" => "index"), array("class" => $this->fetch("activeIndex"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-users'></i> Equipo (subir)", array("controller" => "administrador", "action" => "equipo"), array("class" => $this->fetch("activeEquipo"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-align-left'></i></i> Footer", array("controller" => "administrador", "action" => "footer"), array("class" => $this->fetch("activeFooter"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-home'></i> Inicio", array("controller" => "administrador", "action" => "inicio"), array("class" => $this->fetch("activeInicio"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-coffee'></i> Nuestro Estudio", array("controller" => "administrador", "action" => "nuestro_estudio"), array("class" => $this->fetch("activeNuestro"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-pencil'></i> Áreas de práctica", array("controller" => "administrador", "action" => "area_de_practica"), array("class" => $this->fetch("activePractica"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-users'></i> Equipo", array("controller" => "administrador", "action" => "equipo2"), array("class" => $this->fetch("activeEquipo2"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-envelope-o'></i> Contacto", array("controller" => "administrador", "action" => "contacto"), array("class" => $this->fetch("activeContacto"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-align-justify'></i> Palabras Menu", array("controller" => "administrador", "action" => "palabras_menu"), array("class" => $this->fetch("activePalabras_menu"), "escape" => false)); ?></li>
            <li><?php echo $this->Html->link("<i class='fa fa-outdent'></i> Palabras", array("controller" => "administrador", "action" => "palabras"), array("class" => $this->fetch("activePalabras"), "escape" => false)); ?></li>
        </ul>
    </div>
    <div id="ascrail2000" class="nicescroll-rails" style="width: 3px; z-index: auto; cursor: default; position: absolute; top: 0px; left: 207px; height: 936px; display: block; opacity: 0;"><div style="position: relative; top: 0px; float: right; width: 3px; height: 917px; background-color: rgb(149, 149, 149); border: 0px solid rgb(255, 255, 255); background-clip: padding-box; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px;"></div></div>
    <div id="ascrail2000-hr" class="nicescroll-rails" style="height: 3px; z-index: auto; top: 933px; left: 0px; position: absolute; cursor: default; display: none; opacity: 0; width: 207px;"><div style="position: relative; top: 0px; height: 3px; width: 210px; background-color: rgb(149, 149, 149); border: 0px solid rgb(255, 255, 255); background-clip: padding-box; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; left: 0px;"></div></div>
