<div class="row" style="  margin-right: -30px;">
    <div class="col-xs-12 flex-col">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item">
                    <img src="../img/slider/banner.jpg" alt="Los Angeles">
                </div>
                <div class="item active">
                    <img src="../img/slider/banner0.jpg" style="width: 100%" alt="">
                </div>                     
                <div class="item">
                    <img src="../img/slider/banner1.jpg" alt="">
                </div>                    
                <!--div class="item">
                    <img src="../img/slider/banner2.jpg" alt="Chicago">
                </div-->
                <div class="item">
                    <img src="../img/slider/banner3.jpg" alt="Chicago">
                </div>
                <div class="item">
                    <img src="../img/slider/banner4.jpg" alt="Chicago">
                </div>
                <div class="item">
                    <img src="../img/slider/banner5.jpg" alt="Chicago">
                </div>                                                            
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="border-radius: 50px;padding: 10px;  height: 49px;  width: 49px;  margin-top: 100px;  background: none !important;  margin-left: 25px;  z-index: 99">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next" style="border-radius: 50px;padding: 10px;height: 49px;width: 49px;margin-top: 100px;background: none !important;z-index: 99;margin-right: 25px; z-index: 99">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>