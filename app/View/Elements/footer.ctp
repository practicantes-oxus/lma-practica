<style type="text/css">
  p{
    font-size: 15px;
  }
</style>
<div id="footer">
    <div class="container">
      <div class="row">
         <div class="col-xs-3 text-center">
             <div class="info" style="padding-top: 25px;">
                 <p>Isidora Goyenecha 3120, Of. Piso 16</p>
                 <p>Las Condes, Santiago de Chile</p>
             </div>
         </div>
         <div class="col-xs-3 text-center">
             <div class="info" style="padding-top: 25px;">
                 <p>Orella 610, Piso 17, Of. 1703</p>
                 <p>Antofagasta</p>
             </div>
         </div>
         <div class="col-xs-3 text-center">
             <div class="info" style="padding-top: 25px;">
                 <p>Antonio Varas 216, Piso 10, Of. 1002</p>
                 <p>Puerto Montt</p>
             </div>
         </div>
         <div class="col-xs-3 text-center">
             <div class="info" style="padding-top: 25px;">
                 <p>Manuel Antonio Matta 520, Of. 502</p>
                 <p>Osorno.</p>
             </div>
         </div>
      </div>
    </div>
</div>
