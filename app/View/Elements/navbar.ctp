<div id="menu">
    <div class="row menu_fondo" style="  margin-right: -30px !important;">
        <div class="col-xs-12">
            <div class="container">
                <div class="col-xs-4">
                    <div class="logo_fondo">
                        <div class="logo"></div>
                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="row">
                        <div class="lenguajes" style="display: flex;">
                            <li style="width: 70px; margin-right: 2px;"><?php echo $this->Html->link("ES", array("controller" => "home", "action" => "change", "lang" => "es", "?" => array("continue" => $this->here)), array("class" => $language == "es" ? "active" : "", )); ?></li>

                            <li style="width: 70px; margin-right: 2px"><?php echo $this->Html->link("EN", array("controller" => "home", "action" => "change", "lang" => "en", "?" => array("continue" => $this->here)), array("class" => $language == "en" ? "active" : "", )); ?></li>

                            <li style="width: 70px; margin-right: 2px"><?php echo $this->Html->link("PT", array("controller" => "home", "action" => "change", "lang" => "pt", "?" => array("continue" => $this->here)), array("class" => $language == "pt" ? "active" : "", )); ?></li>

                            <!--li style="width: 70px; margin-right: 2px"><?php echo $this->Html->link("FR", array("controller" => "home", "action" => "change", "lang" => "es", "?" => array("continue" => $this->here)), array("class" => $language == "es" ? "" : "", )); ?></li>

                            <li style="width: 70px; margin-right: 2px"><?php echo $this->Html->link("DE", array("controller" => "home", "action" => "change", "lang" => "es", "?" => array("continue" => $this->here)), array("class" => $language == "es" ? "" : "", )); ?></li-->                                                        
                        </div>
                    </div>
                    <div class="navbar1">
                        <ul class="menu" style="display: flex;  margin-top: 62px;">
                            
                            <li class="<?php echo $this -> fetch("inicio"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["inicio_" . $language], array( "controller" => "home", "action" => "index") ); ?>
                            </li>
                            
                            <li class="<?php echo $this -> fetch("estudio"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["nuestro_estudio_" . $language], array( "controller" => "estudio", "action" => "index") ); ?>
                            </li>
                            
                            <li class="<?php echo $this -> fetch("practica"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["area_practica_" . $language], array( "controller" => "practica", "action" => "index") ); ?></li>
                            
                            <li class="<?php echo $this -> fetch("equipo"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["equipo_" . $language], array( "controller" => "equipo", "action" => "index") ); ?></li>
                            
                            <!-- RED -->
                            <li class="<?php echo $this -> fetch("redes"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["red_" . $language], array( "controller" => "red", "action" => "index") ); ?>
                            </li>    
                            <!-- Fin RED -->    


                            <li class="<?php echo $this -> fetch("novedades"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["novedades_" . $language], array( "controller" => "novedades", "action" => "index") ); ?></li>
                            
                            <li class="<?php echo $this -> fetch("contacto"); ?>">
                                <?php echo $this -> Html -> link( $checkPalabras1 ["PalabrasMenu"]["contacto_" . $language], array( "controller" => "contacto", "action" => "index") ); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
