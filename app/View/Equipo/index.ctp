<?php $this -> append("equipo"); echo "active"; $this -> end(); ?>
<style type="text/css">
    /*.shine{
        -webkit-transition: text-shadow 0.2s linear;-moz-transition: text-shadow 0.2s linear;-ms-transition: text-shadow 0.2s linear;-o-transition: text-shadow 0.2s linear;transition: text-shadow 0.2s linear;
    }
    .shine:hover{
        -webkit-transition: text-shadow 0.2s linear;-moz-transition: text-shadow 0.2s linear;-ms-transition: text-shadow 0.2s linear;-o-transition: text-shadow 0.2s linear;transition: text-shadow 0.2s linear;text-shadow: 0 0 10px white;
    }*/
    a.list-group-item:hover{
        background-color: #fd7600;
        color:#fff;
    }
    a.list-group-item{
        background-color: #fff;
        color: #fd7600;
    }
</style>
<div id="equipo">
    <div class="row"  style="  margin-right: -30px;">
        <div class="container">
            <div class="row" style="margin-bottom: 15px;  margin-top: 14px">
                <div class="col-xs-12">
                    <div class="box-1-equipo">
                        <?php echo $checkEquipo2 ["EquiposDes"]["descrip_" . $language]; ?>
                    </div>
                </div>     
                <div class="col-xs-12" style="margin-bottom:10px;">
                    <div class="col-xs-2" id="search-box">   
                        <?= $this->Form->input('categoria',['options'=>$categoriaList,'empty'=>'Todos','selected'=>'','class'=>'form-control','label'=>''])?> 
                        <?php foreach($checkEquipo as $equipo):?>
                                <a class="name-field list-group-item list-group-item-action" data-categoria="<?= $equipo['Equipo']['categoria']?>" href="#" data-id="<?= $equipo['Equipo']['id']?>" ><?= $equipo['Equipo']['titulo_es'] ?></a>
                        <?php endforeach;?>
                    </div>                        
                    <!--div class="col-xs-2" id="search-box">
                        <div class="list-group" style="max-height: 700px;overflow:auto">
                            <?php foreach ($namesListByCategoria as $categoria => $members):?>
                                <div class="categoria list-group-item list-group-item-action active" style="font-size:90%;color:rgb(200,200,200)"><?= $categoria?></div>
                                    <?php foreach($members as $id => $name):?>
                                        <a class="name-field list-group-item list-group-item-action" href="#" style="font-size: 12px;" data-id="<?= $id?>">
                                            <?= $name ?>        
                                        </a>
                                    <?php endforeach;?>
                            <?php endforeach;?>
                        </div>
                    </div-->
                    <style type="text/css">
                        * {
                            }

                            div#imagen {
                              width: 200px;
                              height: 200px;
                            }

                            div#info  {
                              position:absolute;
                              overflow:hidden;
                              width: 200px;
                              height: 200px;
                              margin-left: -15px;
                              background-color: rgba(253,118,0,0.84);
                              opacity:0;
                              transition: opacity 0.3s;
                            }

                            div#imagen:hover div#info {
                              opacity:1;
                            }

                            p#headline {
                              position: absolute;
                              font-size: 25px;
                              margin-right: 45px;
                              color:#fff;
                              margin-top: 15px;
                              transition: margin-left 0.3s;
                            }

                            div#imagen:hover p#headline {
                              margin-left: 50px;
                            }

                            p#descripcion {
                              font-size: 25px;
                              text-align: center;
                              margin-top: 300px;
                              transition: margin-top 0.3s;
                              color:#fff;
                            }

                            div#imagen:hover p#descripcion {
                              margin-top: 150px;
                            }


                    </style> 
                    <div class="col-xs-10 text-center" id="photo-box" >
                        <div class="row">
                            <?php foreach($checkEquipo as $key => $equipo): 
                                if($equipo["Equipo"]["categoria"]==1){
                                    $cat="Socio";
                                }
                                if($equipo["Equipo"]["categoria"]==2){
                                    $cat="Asociado Senior";
                                }
                                if($equipo["Equipo"]["categoria"]==3){
                                    $cat="Asociado";
                                }                              
                                if(!empty($equipo["Equipo"]["imagen"])){?>
                                    <div id="imagen" class="img_equipo col-xs-3 inner-photo-box " data-id="<?=$equipo["Equipo"]["id"]?>" style="
                                        background-size: cover;/*sólo para ejemplo*/
                                        cursor: pointer;
                                        margin-right: 20px;                                                                             
                                        background-image: url('<?="img/equipo/".$equipo["Equipo"]["imagen"]?>');">
                                        <div id="info">
                                            <center><p id="headline"><?=$equipo["Equipo"]["titulo_es"]?></p></center>
                                            <p id="descripcion"><?=$cat?>.</p>
                                        </div>
                                    </div>
                                    <?}?>
                                <?if (empty($equipo["Equipo"]["imagen"])) {?>
                                    <div id="imagen" class="img_equipo col-xs-3 inner-photo-box" data-id="<?=$equipo["Equipo"]["id"]?>" style="
                                        cursor: pointer;
                                        margin-right: 20px;                                       
                                        background-size: cover;/*sólo para ejemplo*/
                                        background-image: url('<?="img/equipo/default.png"?>');">
                                        <div id="info">
                                            <center><p id="headline"><?=$equipo["Equipo"]["titulo_es"]?></p></center>
                                            <p id="descripcion"><?=$cat?>.</p>
                                        </div>
                                    </div>
                                <?}?>
                            <?endforeach; ?>
                        </div>
                    </div>
                    <!--div class="col-xs-10" id="photo-box" >
                        <div class="row">
                            <?php foreach($checkEquipo as $key => $equipo): ?>
                                    <?if(!empty($equipo["Equipo"]["imagen"]))
                                        //echo $this -> Html ->image("equipo/" . $equipo["Equipo"]["imagen"], array("class" => "img_equipo col-xs-3 inner-photo-box","data-id"=>$equipo["Equipo"]["id"],'style'=>'width: 200px;height:200px; cursor: pointer;'));
                                    if (empty($equipo["Equipo"]["imagen"])) {
                                        //echo $this -> Html ->image("equipo/default.png", array("class" => "img_equipo col-xs-3 inner-photo-box",'title'=>$equipo["Equipo"]["titulo_" . $language],"data-id"=>$equipo["Equipo"]["id"],'style'=>'width: 200px;height:200px; cursor: pointer;'));
                                    }?>
                            <?endforeach; ?>
                        </div>
                    </div-->

                
                    <div class="col-xs-10" id="info-box" style="display:none;">
                        <div class="tab-content">
                            <?php foreach($checkEquipo as $key => $equipo) { ?>
                                <div role="tabpanel" class="tab-pane  <?php echo $key == 0 ? "in active" : ""; ?>" id="<?php echo $equipo["Equipo"]["id"]; ?>">
                                    <div class="box-1">
                                        <i class="fa fa-times pull-right btn btn-box-tool" id="close-info-box"></i>
                                        <div class="scrollbar1">
                                            <div class="stylesOne" id="style-1">
                                                <div class="content">
                                                    <h1><?php echo $equipo["Equipo"]["titulo_" . $language]; ?></h1>
                                                    <p style="font-size: 15px;"><?php echo $equipo["Equipo"]["descripcion_" . $language]; ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="button" class="btn-info2"  style="border: 0;  height: 36px;  color: #fd7500;">
                                            <?php echo $checkPalabras2 ["Palabra"]["ver_" . $language]; ?>
                                        </button>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>                        
                </div>
            </div>                    
        </div>
    </div>
</div>
<?php $this->append("modal"); ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Consultar</h4>
                </div>
                <div class="modal-body">
                    <form  class="form-horizontal" action="<?php echo $this->webroot . "equipo/send"; ?>" method="post" class="form-horizontal">
                        <div class="tab-content">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Email del Abogado:</label>
                                <div class="col-sm-10">
                                    <input type="email"  required name="email"  class="form-control" id="inputPassword3" placeholder="Ej: ejemplo@dominio.cl">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Tu Email:</label>
                                <div class="col-sm-10">
                                    <input type="email" required  name="email2"  class="form-control" id="inputPassword3" placeholder="Ej: ejemplo@dominio.cl">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Asunto:</label>
                                <div class="col-sm-10">
                                    <input type="text" required name="asunto" class="form-control" id="inputPassword3" placeholder="Ej: Roberto Guzman">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Consulta:</label>
                                <div class="col-sm-10">
                                    <textarea  required name="message" class="form-control" id="inputPassword3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-success boton"><?php echo $checkPalabras2 ["Palabra"]["enviar_" . $language]; ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $this->end(); ?>
<?php $this -> append( "script" ); ?>
    <script>
        $(function() {
            $(".stylesOne").mCustomScrollbar();
            $(".stylesTwo").mCustomScrollbar({
                axis: "x",
                autoExpandScrollbar: true,
                advanced: {
                    autoExpandHorizontalScroll: true
                }
            });
            console.log($('input[name="categoria"]'));
            $('#categoria').change(function(){
                if(!($(this).val() == '')){
                    $('.name-field').hide();
                    $('.img_equipo').hide();
                    $('.name-field[data-categoria="'+ $(this).val()+'"]');
                    $('.name-field[data-categoria="'+ $(this).val()+'"]').each(function(){
                        $(this).show(200);
                        $('.img_equipo[data-id="'+ $(this).attr('data-id') +'"]').show(500);
                    });
                }else{
                    $('.name-field').show(200);
                    $('.img_equipo').show(500);
                }
            });
            /*$("#name-search-box").keyup(function(){
                var searchText = $(this).val().toUpperCase();
                $(".name-field").each(function(){
                    var name = $(this).html().toUpperCase();
                    var id = $(this).attr('data-id');
                    if(name.indexOf(searchText) >-1){
                        $('.img_equipo[data-id="'+id +'"]').show(500);
                        $(this).show();
                        //$(this).next().show();
                    }
                    else
                    {
                        $('.img_equipo[data-id="'+id +'"]').hide(500);
                        $(this).hide();
                        //$(this).next().hide();
                    }
                });
                /*$(".categoria").each(function(){

                    if($(this).next().children().find(":visible").length == 0){
                        $(this).hide();
                    }
                    else
                    {
                        $(this).show();
                    }
                });
                console.log($(this).val());*/
            //});
            $('.img_equipo').click(function(){
                $('#search-box').hide(500);
                $('.inner-photo-box').removeClass('col-xs-3');
                $('.inner-photo-box').addClass('col-xs-12');
                $('.img_equipo').removeClass('active');
                $('.img_equipo').not($(this)).hide();
                $(this).addClass('active');
                $('#photo-box').prop('class','col-xs-2');
                $('#info-box').show(500);
                $('.tab-pane').removeClass('in active');
                $(".tab-pane#"+$(this).attr('data-id')).addClass('in active');
            });
            $('.name-field').click(function(){
                $('#search-box').hide(500);
                $('.inner-photo-box').removeClass('col-xs-3');
                $('.inner-photo-box').addClass('col-xs-12');
                $('.img_equipo').removeClass('active');
                $('.img_equipo').not($('.img_equipo[data-id="'+$(this).attr('data-id') +'"]')).hide();
                $('.img_equipo[data-id="'+$(this).attr('data-id') +'"]').addClass('active');
                $('#photo-box').prop('class','col-xs-2');
                $('#info-box').show(500);
                $('.tab-pane').removeClass('in active');
                $(".tab-pane#"+$(this).attr('data-id')).addClass('in active');
            });
            $('.fa-times').click(function(){
                $('#info-box').hide(500);
                $('.img_equipo').addClass('active');
                $('.img_equipo').show();
                $('.inner-photo-box').removeClass('col-xs-12');
                $('.inner-photo-box').addClass('col-xs-3');
                $(".name-field").show();
                $("#name-search-box").val('');
                $('#search-box').show(500);
                $('#photo-box').prop('class','col-xs-10');
            });
        });
        $(".btn-info2").click(function() {
            var button = $(this);
            button.parents(".box-1").find(".cont h1").text();
            $('#myModal').find("form input[name='titulo_es']").val(button.parents(".box-1").find(".cont h1").text());

            $('#myModal').modal("show");
        });
    </script>
<?php $this -> end(); ?>