<?php $this -> append("estudio"); echo "active"; $this -> end(); ?>
<div id="estudio">
    <div class="row"  style="  margin-right: -30px;">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="box-2">
                        <h2><?php echo $checkEstudio ["NuestroEstudio"]["mision_titulo_" . $language]; ?></h2>
                        <?php echo $checkEstudio ["NuestroEstudio"]["mision_descrip_" . $language]; ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="box-2">
                        <h2><?php echo $checkEstudio ["NuestroEstudio"]["vision_titulo_" . $language]; ?></h2>
                        <?php echo $checkEstudio ["NuestroEstudio"]["vision_descrip_" . $language]; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12"><h5><?php echo $checkEstudio ["NuestroEstudio"]["n_valores_" . $language]; ?></h5></div>
            </div>
            <div class="row">

                    <div class="col-xs-3 colm-items" style="height: 150px;border-right: 5px solid #F8F8F8"><h4><?php echo $checkEstudio ["NuestroEstudio"]["confianza_" . $language]; ?></h4>
                        <?php echo $checkEstudio ["NuestroEstudio"]["confianza_des_" . $language]; ?>
                    </div>


                    <div class="col-xs-3 colm-items" style="height: 150px;border-right: 5px solid #F8F8F8"><h4><?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_" . $language]; ?></h4>
                        <?php echo $checkEstudio ["NuestroEstudio"]["eficiencia_des_" . $language]; ?>


                    </div>


                    <div class="col-xs-3 colm-items" style="height: 150px;border-right: 5px solid #F8F8F8"><h4><?php echo $checkEstudio ["NuestroEstudio"]["compromiso_" . $language]; ?></h4>
                        <?php echo $checkEstudio ["NuestroEstudio"]["compromiso_des_" . $language]; ?>

                    </div>


                    <div class="col-xs-3 colm-items" style="height: 150px"><h4><?php echo $checkEstudio ["NuestroEstudio"]["equipo_" . $language]; ?></h4>
                        <?php echo $checkEstudio ["NuestroEstudio"]["equipo_des_" . $language]; ?>
                    </div>

            </div>
        </div>
    </div>
</div>