<?php $this -> append("inicio"); echo "active"; $this -> end(); ?>
<div id="home">
	<div class="loaded">
		<img src="<?php echo $this -> webroot . "img/slider/banner.jpg"; ?>" />
		<img src="<?php echo $this -> webroot . "img/slider/banner0.jpg"; ?>" />
		<img src="<?php echo $this -> webroot . "img/slider/banner1.jpg"; ?>" />
    </div>
    <div class="row" style="  margin-right: -30px;">
            <div class="container">
                <div class="row" style="margin-left: 0px;margin-right: 0px;">
                    <div class="col-xs-4">
                        <div class="box-1">
                            <h1><?php echo $checkInicio ["Inicio"]["titulo1_" . $language]; ?></h1>
                            <?php echo $checkInicio ["Inicio"]["nuestro_estudio_" . $language]; ?>
                            <div class="btn-info2"><?php echo $this -> Html -> link( $checkPalabras2 ["Palabra"]["leer_mas_" . $language], array( "controller" => "estudio", "action" => "index") ); ?></div>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="box-2">
                            <h2><?php echo $checkInicio ["Inicio"]["titulo2_" . $language]; ?></h2>
                            <?php echo $checkInicio ["Inicio"]["trabajo_bono_" . $language]; ?>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="box-3" style="">
                            <h2><?php echo $checkInicio ["Inicio"]["titulo3_" . $language]; ?></h2>
                            <!--<?php echo $checkInicio ["Inicio"]["postulaciones_" . $language]; ?>
                            <button type="button" class="btn-info2" data-toggle="modal" data-target="#myModal" style="  border: 0;  height: 36px;  color: white;">
                                <?php echo $checkPalabras2 ["Palabra"]["haz_click_" . $language]; ?>
                            </button>-->
                            
                            <a href="/red">
                               <center><img src="img/footer/logoall.jpg" style="width:180px; height:60px;"    /></center><!--Logo ALL provisional-->
                            </a>  
                            
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<?php $this->append("modal"); ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">CV</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php echo $this->webroot . "home/send"; ?>" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="tab-content">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Subir CV:</label>
                                <div class="col-sm-10">
                                    <input type="file" name="cv" class="form-control" id="inputPassword3">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="first_name" class="form-control" id="inputPassword3" placeholder="Ej: Roberto Guzman">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Email:</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" id="inputPassword3" placeholder="Ej: ejemplo@dominio.cl">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-success boton"><?php echo $checkPalabras2 ["Palabra"]["enviar_" . $language]; ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $this->end(); ?>
<?php $this -> append( "script" ); ?>
    <script>

    </script>
<?php $this -> end(); ?>