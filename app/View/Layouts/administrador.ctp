<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->Html->css(array('bootstrap.min', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300', 'admin/font-awesome.min', 'admin/style', 'admin/jquerysctipttop', 'admin/DT_bootstrap', 'admin/table', 'admin/summernote', 'animate', 'admin/main'));
		echo $this->fetch('css');
	?>
</head>
<body class="light-theme">
    <?php echo $this->Session->flash("information"); ?>
    <?php echo $this->element("admin/header"); ?>
    <div class="page-container">
        <?php echo $this->element("admin/navbar"); ?>
        <div id="main-content">
            <div class="loading">
                <div class="progress progress-striped active">
                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                </div>
            </div>
            <div class="page-content">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </div>
    <?php echo $this->fetch("modal"); ?>
    <?php
        echo $this->Html->script(array('jquery-1.11.2.min', 'bootstrap.min', 'admin/accordion', 'admin/common-script', 'admin/jquery.nicescroll', 'admin/jquery.sparkline', 'admin/jquery.dataTables', 'admin/DT_bootstrap', 'admin/dynamic_table_init', 'admin/summernote.min', 'admin/bootstrap-wizard.min', 'admin/form-wizard', 'admin/main'));
        echo $this->fetch('extra-script');
        echo $this->element('admin/eliminar');
    ?>
</body>
</html>
