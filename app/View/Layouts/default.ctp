<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		LMA - <?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
        echo $this->fetch('meta');
		echo $this->Html->css(array('admin/font-awesome.min','bootstrap.min', 'jquery.mCustomScrollbar.min.css', 'main', 'customize'));
		echo $this->fetch('css');
	?>
</head>
<body>
<div class="preloadp"></div>
<div class="container-fluid">
	<?php echo $this->element("navbar");
        echo $this->element("carrousel");
    	echo $this->Session->flash();
    	echo $this->fetch('content'); ?>

</div>
    <div style="margin-top: 100px;">
    </div>
    <?php

        echo $this->Html->script(array('jquery-1.11.2.min','bootstrap.min', 'jquery.mCustomScrollbar.concat.min'/*, 'pace.min.js', 'main'*/));
        echo $this->fetch('script');
        echo $this->fetch('modal');
    ?>
    <br><br>
</body>
<br>
<?php echo $this->element("footer"); ?>
</html>
