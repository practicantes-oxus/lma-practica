<?php $this -> append("novedades"); echo "active"; $this -> end(); ?>
<div id="interior">
    <div class="row" style="  margin-right: -30px;">
        <div class="col-xs-12 flex-col">
            <div class="slider"></div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-4">
        <div class="foto_int" style="background-image: url('<?php echo $this -> webroot . "img/novedades/" . $checkNovedade ["Novedade"]["imagen_interior"]; ?>'); height: 220px;  margin-top: 20px;background-size: cover"></div>
                    </div>
                    <div class="col-xs-8">
                        <div class="noticia">
                            <h2><?php echo $checkNovedade ["Novedade"]["titulo_es"]; ?></h2>
                            <div class="box-2-text">
                                <?php echo $checkNovedade ["Novedade"]["descripcion_" . $language]; ?>
                            </div>
                            <div class="btn-info2"><?php echo $this -> Html -> link( $checkPalabras2 ["Palabra"]["volver_" . $language], array( "controller" => "novedades", "action" => "index") ); ?></div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>