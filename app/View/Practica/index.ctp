<?php $this -> append("practica"); echo "active"; $this -> end(); ?>
<div id="practica">
    <div class="row"  style="  margin-right: -30px;">
        <div class="container">
            <div class="row" style="margin-left: 0px;margin-right: 0px">
                <div class="col-xs-4">
                    <div class="box-1">
                        <h1><?php echo $checkArea ["AreaDePractica"]["titulo_area_" . $language]; ?></h1>
                        <?php echo $checkArea ["AreaDePractica"]["area_descrip_" . $language]; ?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="caja">
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t1_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t1_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t5_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t5_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t2_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t2_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t4_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t4_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t6_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t6_des_" . $language]; ?>
                        </div>                        
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="caja" style="margin-bottom: 70px;">
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t3_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t3_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t7_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t7_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t8_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t8_des_" . $language]; ?>
                        </div>
                        <div class="box-2">
                            <h2><?php echo $checkArea ["AreaDePractica"]["t9_" . $language]; ?></h2>
                            <?php echo $checkArea ["AreaDePractica"]["t9_des_" . $language]; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>