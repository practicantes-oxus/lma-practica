<?php $this -> append("redes"); echo "active"; $this -> end(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<div id="redes">
    <div class="row"  style="  margin-right: -30px;">
        <div class="container" style="/*margin-bottom: 100px;*/">
            <div class="row">
                <div class="col-xs-6 ">
                    <div class="box-2 " style="height: 340px;">
                        <h1 class="text-center" style="color: #fd7600;"><?php echo $checkArea ["Redes"]["titulo1_" . $language]; ?></h1>
                        <hr>
                        <p style="font-size: 15px;"><?php echo $checkArea ["Redes"]["desc1_" . $language]; ?></p>
                        <div class="info">
                            <div class="box-2">
                                <br>
                                <p>&bull;Isidora Goyenecha 3120, Piso 16 Las Condes, Santiago de Chile.</p>
                                <p>&bull;Orella 610, Piso 17, Of. 1703 Antofagasta.</p>
                                <p>&bull;Antonio Varas 216, Piso 10, Of. 1002 Puerto Montt</p>
                                <p>&bull;Manuel Antonio Matta 520, Of. 502 Osorno.</p>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="col-xs-6 ">
                    <div class="box-2 " style="height: 340px;">
                        <?=$this -> Html ->image("footer/logoallred.jpg", array("class" => "img_equipo col-xs-12 inner-photo-box",'style'=>'width: 350px;height:70px;margin-left:23%;margin-top:3%;margin-bottom:1%;'));?>
                        <br><br><br><hr>
                        <?php echo $checkArea ["Redes"]["desc2_" . $language]; ?>

                        <a class="caca" style="width:53%;height:95px; margin-top: 30px;" href="http://www.nwadv.com.br/"><?=$this -> Html ->image("footer/logohome.png", array("class" => "img_equipo col-xs-6 inner-photo-box caca",'style' => 'width: 320px;height: 97px;'));?></a>

                        <a class="caca"  style="width:46%;height:140px;" href="#"><?=$this -> Html ->image("footer/logo.png", array("class" => "img_equipo col-xs-6 inner-photo-box caca", 'style' => 'width: 285px;height: 148px;'));?></a>

                        <div class="col-xs-6 text-center">
                            <a class="especial"  href="https://www.brickabogados.com/"><?=$this -> Html ->image("footer/1brick.png", array("class" => "img_equipo inner-photo-box especial", 'style' => 'width: 250px;height: 68px; margin-top:50px;'));?></a>
                        </div>
                        <div class="col-xs-6 text-center">
                            <a class="especial" href="http://www.tytl.com.pe/"><?=$this -> Html ->image("footer/logolyl.png", array("class" => "img_equipo inner-photo-box especial", 'style' => 'width: 250px;height: 118px;'));?></a>
                        </div>

                        <div class="info">
                            <div class="box-2">                                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>