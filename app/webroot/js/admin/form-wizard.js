jQuery(document).ready(function(){
  $('#progressWizard').bootstrapWizard({
    'nextSelector': '.next',
    'previousSelector': '.previous',
    onNext: function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index+1;
      var $percent = ($current/$total) * 100;
      if($current == $total) {
        $(".modal-footer").fadeIn();
      }
      jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
    },
    onPrevious: function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index+1;
      var $percent = ($current/$total) * 100;
      if($current != $total) {
        $(".modal-footer").hide(0);
      }
      jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
    },
    onTabShow: function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index+1;
      var $percent = ($current/$total) * 100;
      if($current == $total) {
        $(".modal-footer").fadeIn();
      }
      jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
    }
  });
$('#progressWizard_edit').bootstrapWizard({
    'nextSelector': '.next',
    'previousSelector': '.previous',
    onNext: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        if($current == $total) {
            $(".modal-footer").fadeIn();
        }
        jQuery('#progressWizard_edit').find('.progress-bar').css('width', $percent+'%');
    },
    onPrevious: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        if($current != $total) {
            $(".modal-footer").hide(0);
        }
        jQuery('#progressWizard_edit').find('.progress-bar').css('width', $percent+'%');
    },
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        if($current == $total) {
            $(".modal-footer").fadeIn();
        }
        jQuery('#progressWizard_edit').find('.progress-bar').css('width', $percent+'%');
    }
});
});