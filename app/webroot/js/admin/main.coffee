adminApp = (->
	initJS = ->
		$("[data-toggle='tooltip']").tooltip
			container: "body"
		$("#summernote, #summernote_two, #summernote_three, #summernote_four").summernote
			height: 200
		$(".leftside-navigation ul li a ").each ->
			$(@).click ->
				button = $ @
				$("#main-content .page-content").fadeOut(0)
				$("#main-content .loading").fadeIn(0)
				return
			return
		return
	casesJS = ->
		$(".btn-save_case").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.content").code()
			button.parents(".modal").find("form textarea").val content
			button.parents(".modal").find("form").submit()
			return
		$(".btn-save_case_edit").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.content.edit").code()
			button.parents(".modal").find("form textarea").val content
			button.parents(".modal").find("form").submit()
			return
		$(".btn-show_image").each ->
			$(@).click ->
				button = $ @
				data_id = button.attr "data-id"
				data_row = button.attr "data-row"
				data_type = button.attr "data-type"
				data_target = button.attr "data-target"
				data_base = button.attr "data-base"
				image = button.text()
				$(data_target).find(".modal-body input[name='id']").val data_id
				$(data_target).find(".modal-body input[name='type']").val data_type
				$(data_target).find(".modal-body input[type='file']").attr "name", data_row
				$(data_target).find(".modal-body img").attr "src", window.location.origin + data_base + "img/cases/thumbnail/" + image
				return
			return
		$(".btn-show_image_primary").each ->
			$(@).click ->
				button = $ @
				data_id = button.attr "data-id"
				data_row = button.attr "data-row"
				data_type = button.attr "data-type"
				data_target = button.attr "data-target"
				data_base = button.attr "data-base"
				image = button.text()
				$(data_target).find(".modal-body input[name='id']").val data_id
				$(data_target).find(".modal-body input[name='type']").val data_type
				$(data_target).find(".modal-body input[type='file']").attr "name", data_row
				$(data_target).find(".modal-body img").attr "src", window.location.origin + data_base + "img/cases/" + image
				return
			return
		$(".btn-edit_case").each ->
			$(@).click ->
				button = $ @
				data_target = $("#edit_case")
				data_target.find("form input[name='id']").val button.attr "data-id"
				data_target.find("form input[name='client']").val button.parents("tr").find("td.client").text()
				data_target.find("form input[name='case']").val button.parents("tr").find("td.case").text()
				data_target.find("form select[name='industry'] option[value='" + button.parents("tr").find("td.industry").text() + "']").attr "selected", "selected"
				data_target.find("form select[name='speciality'] option[value='" + button.parents("tr").find("td.speciality").text() + "']").attr "selected", "selected"
				data_target.find("form #summernote.content").code button.parents("tr").find("td.content").text()
				data_target.modal "show"
				return
			return
		$(".btn-save_case_image").click ->
			button = $ @
			button.text "Guardando..."
			button.parents(".modal").find("form").submit()
			return
		$(".btn-delete_case").each ->
			$(@).click ->
				button = $ @
				id = button.attr "data-id"
				data_target = $("#delete_image")
				data_target.find("form input[name='id']").val id
				data_target.modal "show"
				return
			return
		$(".btn-save_industry").click ->
			button = $ @
			button.text "Guardando..."
			button.parents(".modal").find("form").submit()
			return
		$(".btn-delete_industry").each ->
			$(@).click ->
				button = $ @
				button.text "Eliminando..."
				id = button.attr "data-id"
				data_target = $("#delete_industry")
				data_target.find("form input[name='id']").val id
				data_target.modal "show"
				return
			return
		$(".btn-save_speciality").click ->
			button = $ @
			button.text "Guardando..."
			button.parents(".modal").find("form").submit()
			return
		$(".btn-delete_speciality").each ->
			$(@).click ->
				button = $ @
				button.text "Eliminando..."
				id = button.attr "data-id"
				data_target = $("#delete_speciality")
				data_target.find("form input[name='id']").val id
				data_target.modal "show"
				return
			return
		return
	challengeJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_content").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_two.content").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		return
	clientJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_image").click ->
			button = $ @
			button.text "Guardando..."
			button.parents(".modal").find("form").submit()
			return
		$(".btn-show_image").each ->
			$(@).click ->
				button = $ @
				data_target = button.attr "data-target"
				data_base = button.attr "data-base"
				image = button.text()
				$(data_target).find(".modal-body img").attr "src", window.location.origin + data_base + "img/clients/" + image
				return
			return
		$(".btn-delete_image").each ->
			$(@).click ->
				button = $ @
				button.text "Eliminando..."
				id = button.attr "data-id"
				image = button.parents("tr").find(".btn-show_image").text()
				data_target = $("#delete_image")
				data_target.find("form input[name='id']").val id
				data_target.find("form input[name='images']").val image
				data_target.modal "show"
				return
			return
		return
	indexJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_image").click ->
			button = $ @
			button.text "Guardando..."
			button.parents(".modal").find("form").submit()
			return
		$(".btn-show_image").each ->
			$(@).click ->
				button = $ @
				data_target = button.attr "data-target"
				data_base = button.attr "data-base"
				image = button.text()
				$(data_target).find(".modal-body img").attr "src", window.location.origin + data_base + "img/home/" + image
				return
			return
		$(".btn-delete_image").each ->
			$(@).click ->
				button = $ @
				button.text "Eliminando..."
				id = button.attr "data-id"
				image = button.parents("tr").find(".btn-show_image").text()
				data_target = $("#delete_image")
				data_target.find("form input[name='id']").val id
				data_target.find("form input[name='images']").val image
				data_target.modal "show"
				return
			return
		return
	methodologyJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_content").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_two.content").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		return
	partnerJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_content").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_two.content").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		return
	portfolioJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_image").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_three.title").code()
			button.parents(".modal").find("form textarea").val content
			button.parents(".modal").find("form").submit()
			return
		$(".btn-save_edit_image").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_four.title").code()
			button.parents(".modal").find("form textarea").val content
			button.parents(".modal").find("form").submit()
			return
		$(".btn-show_image").each ->
			$(@).click ->
				button = $ @
				data_target = button.attr "data-target"
				data_base = button.attr "data-base"
				image = button.text()
				$(data_target).find(".modal-body img").attr "src", window.location.origin + data_base + "img/portfolio/" + image
				return
			return
		$(".btn-edit_image").each ->
			$(@).click ->
				button = $ @
				id = button.attr "data-id"
				text = button.parents("tr").find("td:nth-child(2)").html()
				console.log text
				data_target = $("#edit_image")
				data_target.find("form input[name='id']").val id
				data_target.find("form #summernote_four.title").code text
				data_target.modal "show"
				return
			return
		$(".btn-delete_image").each ->
			$(@).click ->
				button = $ @
				button.text "Eliminando..."
				id = button.attr "data-id"
				image = button.parents("tr").find(".btn-show_image").text()
				data_target = $("#delete_image")
				data_target.find("form input[name='id']").val id
				data_target.find("form input[name='images']").val image
				data_target.modal "show"
				return
			return
		return
	serviceJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_content").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_two.content").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		return
	valueJS = ->
		$(".btn-save_text_left").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote.text_left").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		$(".btn-save_content").click ->
			button = $ @
			button.text "Guardando..."
			content = $("#summernote_two.content").code()
			button.parents("form").find("textarea").val content
			button.parents("form").submit()
			return
		return
	whoweareJS = ->
		return
	init: ->
		initJS()
		return
	cases: ->
		casesJS()
		return
	challenge: ->
		challengeJS()
		return
	client: ->
		clientJS()
		return
	index: ->
		indexJS()
		return
	methodology: ->
		methodologyJS()
		return
	partner: ->
		partnerJS()
		return
	portfolio: ->
		portfolioJS()
		return
	service: ->
		serviceJS()
		return
	value: ->
		valueJS()
		return
	whoweare: ->
		whoweareJS()
		return
)()

$ ->
	adminApp.init()
	return